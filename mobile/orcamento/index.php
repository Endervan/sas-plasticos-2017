<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



function adiciona($id, $tipo_orcamento){
  //  VERIFICO SE O TIPO DE SOLICITACAO E DE SERVICO OU PRODUTO
  switch($tipo_orcamento){
    case "servico":
    //	VERIFICO SE NAO TEM JA ADICIONADO
    if(count($_SESSION[solicitacoes_servicos] > 0)):
      if (!@in_array($id, $_SESSION[solicitacoes_servicos])) :
        $_SESSION[solicitacoes_servicos][] = $id;
      endif;
    else:
      $_SESSION[solicitacoes_servicos][] = $id;
    endif;
    break;


    case "produto":
    //	VERIFICO SE NAO TEM JA ADICIONADO
    if(count($_SESSION[solicitacoes_produtos] > 0)):
      if (!@in_array($id, $_SESSION[solicitacoes_produtos])) :
        $_SESSION[solicitacoes_produtos][] = $id;
      endif;
    else:
      $_SESSION[solicitacoes_produtos][] = $id;
    endif;
    break;
  }
}



function excluir($id, $tipo_orcamento){
  switch ($tipo_orcamento) {
    case 'produto':
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case 'servico':
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
  }
}


//  EXCLUI OU ADD UM ITEM
if($_GET[action] != ''){
  //  SELECIONO O TIPO
  switch($_GET[action]){
    case "add":
    adiciona($_GET[id], $_GET[tipo_orcamento]);
    break;
    case "del":
    excluir($_GET[id], $_GET[tipo_orcamento]);
    break;
  }
}


?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>




  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>


  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input{
    display: none;
  }




  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
  }
  </style>




</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>ORÇAMENTO</h6>
      <amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->


    </div>
  </div>


  <!--  ==============================================================  -->
  <!--   menu categoria + orcaamento -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!--  ==============================================================  -->
  <!--   menu categoria + orcaamento -->
  <!--  ==============================================================  -->


           <?php
           //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
           if(isset($_GET[nome])){
             //  CADASTRO OS PRODUTOS SOLICITADOS
             for($i=0; $i < count($_GET[qtd]); $i++){
               $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_GET[idproduto][$i]);
               $produtos .= "
                               <tr>
                               <td><p>". $_GET[qtd][$i] ."</p></td>
                               <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                               </tr>
                               ";
             }

             //  CADASTRO OS SERVICOS SOLICITADOS
             for($i=0; $i < count($_GET[qtd_servico]); $i++){
               $dados = $obj_site->select_unico("tb_servicos", "idservico", $_GET[idservico][$i]);
               $produtos .= "
                               <tr>
                               <td><p>". $_GET[qtd_servico][$i] ."</p></td>
                               <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                               </tr>
                               ";
             }

             //  ENVIANDO A MENSAGEM PARA O CLIENTE
             $texto_mensagem = "
                                 O seguinte cliente fez uma solicitação pelo site. <br />
                                 Nome: $_GET[nome] <br />
                                 Email: $_GET[email] <br />
                                 Telefone: $_GET[telefone] <br />
                                 Localidade: $_GET[localidade] <br />
                                 Mensagem: <br />
                                 ". nl2br($_GET[mensagem]) ." <br />
                                 <br />
                                 <h2> Produtos selecionados:</h2> <br />
                                 <table width='100%' border='0' cellpadding='5' cellspacing='5'>
                                 <tr>
                                 <td><h4>QTD</h4></td>
                                 <td><h4>PRODUTO</h4></td>
                                 </tr>
                                 $produtos
                                 </table>
                                 ";

             if (Util::envia_email($config[email],("$_GET[nome] solicitou um orçamento"),($texto_mensagem),($_GET[nome]), $_GET[email])) {
                 Util::envia_email($config[email_copia],("$_GET[nome] solicitou um orçamento"),($texto_mensagem),($_GET[nome]), $_GET[email]);
                 unset($_SESSION[solicitacoes_produtos]);
                 unset($_SESSION[solicitacoes_servicos]);
                 $enviado = 'sim';
             }




           }
           ?>




  <div class="row bottom50 fundo_contatos">
    <div class="col-12">

      <?php if($enviado == 'sim'): ?>
          <div class="col-12  text-center top40">
              <h3>Orçamento enviado com sucesso.</h3>
              <a class="btn btn_formulario" href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                ADICIONE PRODUTO(S)
              </a>
          </div>
      <?php else: ?>

        <form method="get" class="p2" action-xhr="envia.php" target="_top">

        <?php require_once('../includes/lista_itens_orcamento.php'); ?>

        <div class="lista-produto-titulo top20">
          <h5 >CONFIRME SEUS DADOS</h5>
        </div>

        <div class="ampstart-input inline-block form_contatos m0 p0 mb3">
          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
            <span class="fa fa-user form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
            <span class="fa fa-phone form-control-feedback"></span>
          </div>

          <div class="relativo">
            <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
            <span class="fa fa-star form-control-feedback"></span>
          </div>

          <?php /*
          <div class="relativo">
          <input type="date" class="input-form input100 block border-none p0 m0" name="data" placeholder="DATA" required>
          <span class="fa fa-calendar form-control-feedback"></span>
          </div>

          <div class="relativo">
          <input type="time" class="input-form input100 block border-none p0 m0" name="hora" placeholder="HORÁRIO" required>
          <span class="fa fa-clock-o form-control-feedback"></span>
          </div>
          */ ?>

          <div class="relativo">
            <textarea name="mensagem" placeholder="OBSERVAÇÕES" class="input-form1 input100 campo-textarea" ></textarea>
            <span class="fa fa-pencil form-control-feedback"></span>
          </div>

        </div>
        <div class="col-12 text-right">
          <?php if(count($_SESSION[solicitacoes_servicos])+count($_SESSION[solicitacoes_produtos]) > 0): ?>
            <input type="submit" value="ENVIAR" class="btn btn_formulario">
          <?php endif; ?>
        </div>


        <div submit-success>
          <template type="amp-mustache">
            Orçamento enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>

    <?php endif; ?>
    </div>
  </div>

  <?php require_once("../includes/rodape.php") ?>

</body>

</html>
