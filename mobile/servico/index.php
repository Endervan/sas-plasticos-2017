
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/servicos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
		background-size:  100% 128px;
	}
	</style>

	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>



</head>

<body class="bg-interna">

	<?php
	$voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>

	<div class="row">
		<div class="col-12 text-center localizacao-pagina">
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
			<h6>SERVICOS</h6>
			<amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
		</div>
	</div>

	<!-- ======================================================================= -->
	<!--LISTA CONTATOS  -->
	<!-- ======================================================================= -->
	<?php require_once("../includes/lista_contatos.php") ?>
	<!-- ======================================================================= -->
	<!--LISTA CONTATOS  -->
	<!-- ======================================================================= -->

	<div class="row">
		<div class="col-12 mont top25">
			<h1 class="text-uppercase"><span>	<?php echo Util::imprime($dados_dentro[titulo]) ?></span></h1>
		</div>

		<div class="col-12">
			<amp-img
			on="tap:lightbox1"
			role="button"
			tabindex="0"
			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>"
			width="158"
			height="94"
			layout="responsive"
			alt="<?php echo Util::imprime($row[titulo]) ?>">
		</amp-img>
		<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>

	</div>
</div>


<div class="row servico_dentro">
	<div class="col-12 top15 dicas_dentro ">
		<div>	<p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
	</div>
</div>

<div class="row top30">
	<div class="col-12">
		<a class="col-8 col-offset-2 btn btn_saiba_mais_conj" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=servico" title="SOLICITAR UM ORÇAMENTO">
			SOLICITE ORÇAMENTO
	</a>
</div>

</div>



<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php require_once("../includes/veja_tambem.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
