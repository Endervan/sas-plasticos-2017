
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>




</head>



<body class="bg-index">


  <div class="row">
    <div class="col-8 col-offset-2 text-center topo">
      <amp-img  src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="41" width="200"></amp-img>
    </div>
  </div>


  <div class=" row font-index ">
<div class="col-12 top35">
  <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/texto_home.png" alt="Home" height="77" width="280"></amp-img>

</div>
  </div>

  <!-- ======================================================================= -->
  <!--  MENU -->
  <!-- ======================================================================= -->
  <div class="row">

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/contatos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contato_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top30">
      <a href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde_geral.png"
          width="95"
          height="95"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>


  </div>
  <!-- ======================================================================= -->
  <!--  MENU -->
  <!-- ======================================================================= -->


</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
