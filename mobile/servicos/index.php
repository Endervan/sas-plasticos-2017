<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>






</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>SERVICOS</h6>
      <amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->
  <div class="row bg_servicos">

    <div class="bg_empresa">
    <?php
    $result = $obj_site->select("tb_servicos");
    if(mysql_num_rows($result) > 0){
      $i = 0;
      while($row = mysql_fetch_array($result)){


        ?>


        <div class="col-12 top20"><h3 class=" titulo_servicos_home text-uppercase"><?php Util::imprime($row[titulo]); ?></h3></div>
        <div class="col-6 top5">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <amp-img
            src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
            width="158"
            height="94"
            layout="responsive"
            alt="<?php echo Util::imprime($row[titulo]) ?>">
          </amp-img>
        </a>
      </div>
      <div class="col-6 top10">
        <div class=" desc_conj mont"><p><?php Util::imprime($row[descricao]); ?></p></div>
        <div class="top10 text-right">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais_conj">MAIS DETALHES</a>
        </div>
      </div>


      <?php
      if ($i==0) {
        echo "<div class='clearfix'>  </div>";
      }else {
        $i++;
      }
    }
  }
  ?>
  </div>

</div>
<!--  ==============================================================  -->
<!--   SERVICOS -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php require_once("../includes/veja_tambem.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
