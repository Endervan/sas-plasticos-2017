<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>

  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 1rem /* 16/16 */;
  }
  form.amp-form-submit-success [submit-success] {
    color: green;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success.hide-inputs > input {
    display: none;
  }


  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15);
  ?>
  .bg-interna{
    background:  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php  require_once("../includes/topo.php")?>

  <div class="row">
    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>CONTATO</h6>
      <amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->



  <div class="row bottom30 fundo_contatos">
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12">


      <?php
      //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_POST[nome])){
        $texto_mensagem = "
        Nome: ".($_POST[nome])." <br />
        Email: ".($_POST[email])." <br />
        Telefone: ".($_POST[telefone])." <br />
        Assunto: ".($_POST[assunto])." <br />


        Mensagem: <br />
        ".(nl2br($_POST[mensagem]))."
        ";

        if(Util::envia_email($config[email],("$_POST[nome] solicitou contato pelo site"),($texto_mensagem),($_POST[nome]), $_POST[email])){
          Util::envia_email($config[email_copia],("$_POST[nome] solicitou contato pelo site"),($texto_mensagem),($_POST[nome]), $_POST[email]);
          $enviado = 'sim';
          unset($_POST);
        }




      }
      ?>


      <?php if($enviado == 'sim'): ?>
        <div class="col-12  text-center top40">
          <h3>Email enviado com sucesso.</h3>
          <a class="btn btn_formulario" href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">
            ENVIE OUTRO EMAIL
          </a>
        </div>
      <?php else: ?>

        <form method="post" class="p2" action-xhr="index.php" target="_top">

          <div class="top20 mont">
            <h5>ENVIE SUA MENSAGEM</h5>
          </div>
          <div class="ampstart-input top30 inline-block  form_contatos m0 p0 mb3">
            <div class="relativo">
              <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
              <span class="fa fa-user form-control-feedback"></span>
            </div>

            <div class="relativo">
              <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
              <span class="fa fa-envelope form-control-feedback"></span>
            </div>

            <div class="relativo">
              <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
              <span class="fa fa-phone form-control-feedback"></span>
            </div>

            <div class="relativo">
              <input type="text" class="input-form input100 block border-none p0 m0" name="assunto" placeholder="ASSUNTO" required>
              <span class="fa fa-star form-control-feedback"></span>
            </div>

            <div class="relativo">
              <textarea name="mensagem" placeholder="MENSAGEM" class="input-form1 input100 campo-textarea" ></textarea>
              <span class="fa fa-pencil form-control-feedback"></span>
            </div>

          </div>

          <div class="col-12  relativo text-right bottom40 padding0">
            <div class=" ">
              <button type="submit"
              value="OK"
              class="ampstart-btn caps btn btn_formulario">ENVIAR</button>
            </div>
          </div>


          <div submit-success>
            <template type="amp-mustache">
              Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
            </template>
          </div>

          <div submit-error>
            <template type="amp-mustache">
              Houve um erro, {{name}} por favor tente novamente.
            </template>
          </div>

        </form>

      <?php endif; ?>
    </div>
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->

  </div>


  <!-- ======================================================================= -->
  <!-- mapa   -->
  <!-- ======================================================================= -->
  <div class="row relativo top40">
    <div class="col-6 onde padding0">
      <amp-img class="top10 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/onde.png"  alt="" height="60" width="250"></amp-img>
    </div>

    <div class="col-12 top5 padding0">
      <amp-iframe
      width="480"
      height="194"
      layout="responsive"
      sandbox="allow-scripts allow-same-origin allow-popups"
      frameborder="0"
      src="<?php Util::imprime($config[src_place]); ?>">
    </amp-iframe>
  </div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php require_once("../includes/veja_tambem.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
