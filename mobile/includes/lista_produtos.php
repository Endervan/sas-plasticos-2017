
<!--  ==============================================================  -->
<!--   LISTAGEM PRODUTOS -->
<!--  ==============================================================  -->
<?php
$i = 0;
if(mysql_num_rows($result) == 0){
  echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
}else{
  while($row = mysql_fetch_array($result)){
    ?>

    <div class="col-6 carroucel_produtos mont top10">
      <div class="card">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
          <amp-img
          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
          width="158"
          height="121"
          layout="responsive"
          alt="<?php echo Util::imprime($row[titulo]) ?>">
        </amp-img>
      </a>
      <div class="card-body bg_produtos_home">
        <div class="desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
        <div class=" top5 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[quantidade]); ?></h1></div>

        <div class="d-flex">
          <div class="ml-auto">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>
            <a class="btn btn_produtos_home" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $row[0] ?>&tipo_orcamento=produto" title="SOLICITAR UM ORÇAMENTO">
              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            </a>
          </div>
        </div>

      </div>


    </div>
  </div>


  <?php
  if($i == 1){
    echo '<div class="clearfix"></div>';
    $i = 0;
  }else{
    $i++;
  }

}
}
?>


<!--  ==============================================================  -->
<!--   LISTAGEM PRODUTOS -->
<!--  ==============================================================  -->
