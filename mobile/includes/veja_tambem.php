<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row bottom50">
  <div class="col-8 top40">

    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/produtos_servicos.png"
      width="218"
      height="44"
      layout="responsive"
      alt="AMP">
    </amp-img>

  </div>

  <div class="clearfix">  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
        width="89"
        height="89"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
        width="89"
        height="89"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>


  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
        width="89"
        height="89"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
