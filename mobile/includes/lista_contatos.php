<div class="row">
  <button class="btn btn_agora col-6 padding0"
  on="tap:my-lightbox444"
  role="a"
  tabindex="0">
  <i class="fa fa-phone mr-3" aria-hidden="true"></i>LIGUE AGORA <i class="fa fa-caret-down ml-3" aria-hidden="true"></i>
</button>


<a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
  <button class="btn btn_agora btn_orcamento col-6 padding0">
    SOLICITAR ORÇAMENTO<i class="fa fa-shopping-cart ml-1" aria-hidden="true"></i>
  </button>
</a>

<amp-lightbox id="my-lightbox444" layout="nodisplay">
  <div class="lightbox" role="a" on="tap:my-lightbox444.close" tabindex="0">

    <div class="col-12">
      <a class="btn btn_fechar bottom50" on="tap:my-lightbox444.close">X</a>

      <div class="col-12 padding0 bg_branco_borde">
        <h4 class="top5 col-8">
          <a class="btn_ligue numero" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
            <?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
          </a>
        </h4>
        <a class="btn btn_ligue col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
          CHAMAR
        </a>
      </div>

      <?php if (!empty($config[telefone2])) :?>
        <div class="col-12 padding0 bg_branco_borde">
          <h4 class="top5 col-8">
            <a class="btn_ligue numero" href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
              <?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>
            </a>
          </h4>
          <a class="btn btn_ligue col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd2]); ?><?php Util::imprime($config[telefone2]); ?>">
            CHAMAR
          </a>
        </div>
      <?php endif; ?>



      <?php if (!empty($config[telefone3])): ?>
        <div class="col-12 padding0 bg_branco_borde">
          <h4 class="top5 col-8">
            <a class="btn_ligue numero" href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
              <?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>
            </a>
          </h4>
          <a class="btn btn_ligue col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd3]); ?><?php Util::imprime($config[telefone3]); ?>">
            CHAMAR
          </a>
        </div>
      <?php endif; ?>


      <?php if (!empty($config[telefone4])): ?>
        <div class="col-12 padding0 bg_branco_borde">
          <h4 class="top5 col-8">
            <a class="btn_ligue numero" href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
              <?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>
            </a>
          </h4>
          <a class="btn btn_ligue col-4 padding0" href="tel:+55<?php Util::imprime($config[ddd4]); ?><?php Util::imprime($config[telefone4]); ?>">
            CHAMAR
          </a>
        </div>
      <?php endif; ?>


    </div>

  </div>
</amp-lightbox>


</div>
