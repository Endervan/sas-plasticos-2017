<div class="row">
  <div class="separador-rodape clearfix"></div>
  <div class="rodape col-12 padding0">

    <div class="col-6">
      <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
        <i class="fa fa-phone mr-3" aria-hidden="true"></i>LIGUE AGORA
      </a>
    </div>

    <div class="col-6 text-right">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
          MEU ORÇAMENTO<i class="fa fa-shopping-cart ml-1" aria-hidden="true"></i>
        </a>

      </a>
    </div>
  </div>
</div>
