<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>A EMPRESA</h6>
      <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/lista_contatos.php") ?>
  <!-- ======================================================================= -->
  <!--LISTA CONTATOS  -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row  relativo">
    <div class="bg_empresa">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
      <div class="top35 col-12">
        <h4><?php Util::imprime($row[titulo]); ?></h4>
        <h4><span>EMBALAGENS PLÁSTICAS I NO EMBALO DA VIDA</span></h4>
        <h1> QUEM SOMOS </h1>
      </div>
      <div class="col-12">
        <div class="top30">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--   sustetabilidade -->
  <!--  ==============================================================  -->
  <div class="row">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

    <div class="col-12 top10">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/bg_sustetabilidade.png" alt="" layout="responsive" height="107" width="360"></amp-img>
    </div>
    <div class="col-12">
      <div class="desc_empresa_home top25">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   sustetabilidade -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja_tambem.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->



  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
