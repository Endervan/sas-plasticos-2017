<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 124px center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-12 top90 bottom55">
        <h1><span>FALE CONOSCO</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"> <i class="fa fa-home mr-2"></i>Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Fale Conosco</li>
              </ol>
            </nav>
          </div>

          <div class="col-3 text-right ml-auto">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>


  <div class="container-fluid bg_contatos">
    <div class="row">




      <div class="container ">
        <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="row ">
            <div class="col-4 mont">
              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="col-8 top180 ml-auto">
                <h5>SE PREFERIR</h5>
                <div class="">
                  <h5>LIGUE<span class="ml-2"><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></span></h5>
                </div>
              </div>
            </div>
            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->


            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->
            <div class="col-8 top45 fundo_formulario">

              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback"></span>
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback"></span>
                  </div>
                </div>

                <div class="col">
                  <div class="form-group  icon_form">
                    <input type="text" name="assunto" class="form-control fundo-form form-control-lg input-lg" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="form-row">
                <div class="col">
                  <div class="form-group icon_form">
                    <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form1" placeholder="MENSAGEM"></textarea>
                    <span class="fa fa-pencil form-control-feedback"></span>
                  </div>
                </div>
              </div>


              <div class="col-12 mont text-right padding0 ">
                <button type="submit" class="btn btn_formulario" name="btn_contato">
                  ENVIAR MENSAGEM
                </button>
              </div>


            </div>


          </div>
        </form>
        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->



      </div>

    </div>
  </div>



  <div class="container-fluid">
    <div class="row">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="381" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->

    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>




<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
                  {
                    $texto_mensagem = "
                    Nome: ".($_POST[nome])." <br />
                    Email: ".($_POST[email])." <br />
                    Telefone: ".($_POST[telefone])." <br />
                    Assunto: ".($_POST[assunto])." <br />

                    Mensagem: <br />
                    ".(nl2br($_POST[mensagem]))."
                    ";



                    if (Util::envia_email($config[email],("$_POST[nome] solicitou contato pelo site"),($texto_mensagem),($_POST[nome]), $_POST[email])) {
                      Util::envia_email($config[email_copia],("$_POST[nome] solicitou contato pelo site"),($texto_mensagem),($_POST[nome]), $_POST[email]);
                      Util::alert_bootstrap("Obrigado por entrar em contato.");
                      unset($_POST);
                    }else{
                      Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
                    }

                }


?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
