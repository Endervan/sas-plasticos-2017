<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top65 bottom70">
        <h1><span>NOSSA EMPRESA</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"> <i class="fa fa-home mr-2"></i>Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Empresa</li>
              </ol>
            </nav>
          </div>

          <div class="col-3 text-right ml-auto">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>
      </div>
    </div>
    </div>



    <!--  ==============================================================  -->
    <!--   EMPRESA GERAL -->
    <!--  ==============================================================  -->
    <div class="container-fluid bg_empresa_geral">
      <div class="row">
        <div class="container mont bottom10 top30">
          <div class="row">

            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
            <div class="col-10 ml-auto">
            <div class="w-50">
              <h4><?php Util::imprime($row[titulo]); ?></h4>
              <!-- <h4 class="text-uppercase"><span>embalagens plásticas i no embalo da vida </span></h4> -->
              <div class=" top40">
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>
            </div>
            </div>


          </div>
        </div>
      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   EMPRESA GERAL -->
    <!--  ==============================================================  -->



    <!--  ==============================================================  -->
    <!--   EMPRESA SUSTENTABILIDADE -->
    <!--  ==============================================================  -->
    <div class="container-fluid bg_sustentabilidade1">
      <div class="row">

        <div class="container mont bottom10 top5">
          <div class="row">

            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
            <div class="col-9 ml-auto"><h4 class="ml-5"><?php Util::imprime($row[titulo]); ?></h4></div>
            <div class="col-6 ml-auto">
              <div class="desc_empresa_home top75">
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>



            </div>
          </div>
        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   EMPRESA SUSTENTABILIDADE -->
    <!--  ==============================================================  -->




    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>

  <script type="text/javascript">
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      controlNav: false,/*tira bolinhas*/
      itemWidth: 230,
      itemMargin: 0,
      inItems: 1,
      maxItems: 20
    });
  });
</script>
