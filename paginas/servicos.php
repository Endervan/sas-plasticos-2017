<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: #fafbfd url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top65 bottom70">
        <h1><span>NOSSOS SERVIÇOS</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"> <i class="fa fa-home mr-2"></i>Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Serviços</li>
              </ol>
            </nav>
          </div>

          <div class="col-3 text-right ml-auto">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>





  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_servicos">
    <div class="row">

      <div class="container">
        <div class="row mont">
          <?php
          $result = $obj_site->select("tb_servicos");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while($row = mysql_fetch_array($result)){
              if ($i == 0) {
                $margem = 'ml-auto';
                $i++;
              }else{
                $margem = '';
                $i = 0;
              }

              ?>

              <div class="col-5 <?php echo $margem; ?> top50">
                <div class="row">
                  <div class="col-12"><h3 class=" titulo_servicos_home text-uppercase"><span><?php Util::imprime($row[titulo]); ?></span></h3></div>
                  <div class="col-6 top5">
                    <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 214, 120, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                    </a>
                  </div>
                  <div class="col-6">
                    <div class=" desc_conj "><p><?php Util::imprime($row[descricao]); ?></p></div>
                    <div class="top20 text-right">
                      <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais_conj">MAIS DETALHES</a>
                    </div>
                  </div>
                </div>
              </div>

              <?php
              if ($i==0) {
                echo "<div class='clearfix'>  </div>";
              }else {
                $i++;
              }
            }
          }
          ?>

        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--   VEJA MAIS -->
  <!--  ==============================================================  -->
  <div class="container-fluid relativo bottom40">
    <div class="row">


      <div class="bg_veja">  </div>


      <div class="container">
        <div class="row  ">
          <div class="col-11  ml-auto text-center mont titulo_paginas">
            <h1>CONFIRA TAMBÉM</h1>
            <h1><span>NOSSOS PRODUTOS</span></h1>
          </div>

          <div class="col-12">  </div>

          <?php
          $result = $obj_site->select("tb_produtos","order by rand() limit 4");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              $i=0;
              ?>

              <div class="col-3 carroucel_produtos top45">
                <div class="card">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 253, 202, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                  </a>
                  <div class="card-body bg_produtos_home">
                    <div class="desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
                    <div class=" top5 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[quantidade]); ?></h1></div>

                    <div class="col-9 padding0 ml-auto">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>
                      <a href="javascript:void(0);" class="btn btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg" alt="">
                      </a>
                    </div>

                  </div>


                </div>
              </div>

              <?php

              $i++;

            }
          }
          ?>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   VEJA MAIS -->
  <!--  ==============================================================  -->

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
