<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",5) ?>
<style>
.bg-interna{
  background: #fafbfd url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top65 bottom70">
        <h1 class="<ml-5></ml-5>"><span>NOSSOS PRODUTOS</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo Util::caminho_projeto() ?>/"> <i class="fa fa-home mr-2"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>">Produtos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php Util::imprime($dados_dentro[titulo]); ?></li>
              </ol>
            </nav>
          </div>

        </div>
      </div>
    </div>
  </div>




  <!--  ==============================================================  -->
  <!--  DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container top40 bottom60">
    <div class="row produto_dentro">

      <div class="col-6 order-12 mont top10 servico_Dentro">
        <h1 class="text-uppercase"><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h1>
        <div class="top35">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
        <a href="javascript:void(0);" class="btn btn_orcamento_servico col-6 top60" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
          SOLICITE ORÇAMENTO
        </a>
      </div>


      <div class="col-6 padding0 order-1">

        <!-- ======================================================================= -->
        <!-- SLIDER CATEGORIA -->
        <!-- ======================================================================= -->
        <div class="col-12  slider_prod_geral">


          <!-- Place somewhere in the <body> of your page -->
          <div id="slider" class="flexslider">

            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) == 0){?>

              <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 566, 406, array("class"=>"", "alt"=>"$row[titulo]")) ?>

              <?php
            }else{
              ?>

              <div class="zomm">  </div>


              <!-- <span>Clique na imagem para Ampliar</span> -->
              <ul class="slides slider-prod">

                <?php
                $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
                if(mysql_num_rows($result) > 0)
                {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <li class="zoom">
                      <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4">
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 566, 406, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                        <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" class="input100" /> -->
                      </a>
                    </li>
                    <?php
                  }
                }
                ?>
                <!-- items mirrored twice, total of 12 -->
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides slider-prod-tumb">
                <?php
                $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
                if(mysql_num_rows($result) > 0)
                {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <li>
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />
                    </li>
                    <?php
                  }
                }

              }
              ?>
              <!-- items mirrored twice, total of 12 -->
            </ul>
          </div>

        </div>




      </div>



    </div>

  </div>
  <!--  ==============================================================  -->
  <!--   DESCRICAO -->
  <!--  ==============================================================  -->






  <!--  ==============================================================  -->
  <!--   VEJA MAIS -->
  <!--  ==============================================================  -->
  <div class="container-fluid relativo bottom40 top20">
    <div class="row">
      <div class="bg_veja1">  </div>
      <div class="container">
        <div class="row  ">
          <div class="col-10 ml-auto mont titulo_paginas">
            <h1>CONFIRA TAMBÉM</h1>
            <h1><span>OUTROS PRODUTOS</span></h1>
          </div>

          <div class="col-12">  </div>

          <?php
          $result = $obj_site->select("tb_produtos","order by rand() limit 4");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              $i=0;
              ?>

              <div class="col-3 carroucel_produtos top45">
                <div class="card">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 253, 202, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                  </a>
                  <div class="card-body bg_produtos_home">
                    <div class="desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
                    <div class=" top5 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[quantidade]); ?></h1></div>

                    <div class="col-9 padding0 ml-auto">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>
                      <a href="javascript:void(0);" class="btn btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg" alt="">
                      </a>
                    </div>

                  </div>


                </div>
              </div>

              <?php

              $i++;

            }
          }
          ?>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   VEJA MAIS -->
  <!--  ==============================================================  -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>




<?php require_once('./includes/js_css.php') ?>





<script type="text/javascript">
$(window).load(function() {


  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    sync: "#carousel"
  });


});
</script>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
