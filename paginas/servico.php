<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 124px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <?php
  $voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top65 bottom70">
        <h1><span>NOSSOS SERVIÇOS</span></h1>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo Util::caminho_projeto() ?>/"> <i class="fa fa-home mr-2"></i>Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>">Serviços</a></li>
                <li class="breadcrumb-item active" aria-current="page">Serviço</li>
              </ol>
            </nav>
          </div>

        </div>
      </div>
    </div>
  </div>





    <!-- ======================================================================= -->
    <!--SERVICO DENTRO-->
    <!-- ======================================================================= -->
    <div class="container-fluid bg_servico">
      <div class="row">

        <div class='container'>
          <div class="row top20">

            <div class="col-7 top80">
              <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",548, 308, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>
            </div>
            <div class="col-5 mont top45 servico_Dentro">
              <h1 class="text-uppercase"><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h1>
              <div class="top35">
                <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
              </div>
              <a href="javascript:void(0);" class="btn btn_orcamento_servico col-9 top60" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
                ADICIONAR AO ORÇAMENTO
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!--SERVICO DENTRO-->
    <!-- ======================================================================= -->



    <!--  ==============================================================  -->
    <!--   VEJA MAIS -->
    <!--  ==============================================================  -->
    <div class="container-fluid relativo bottom40 top20">
      <div class="row">
        <div class="bg_veja1">  </div>
        <div class="container">
          <div class="row  ">
            <div class="col-10 ml-auto mont titulo_paginas">
              <h1>CONFIRA TAMBÉM</h1>
              <h1><span>NOSSOS PRODUTOS</span></h1>
            </div>

            <div class="col-12">  </div>

            <?php
            $result = $obj_site->select("tb_produtos","order by rand() limit 4");
            if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
                $i=0;
                ?>

                <div class="col-3 carroucel_produtos top45">
                  <div class="card">
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 253, 202, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                    </a>
                    <div class="card-body bg_produtos_home">
                      <div class="desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
                      <div class=" top5 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[quantidade]); ?></h1></div>

                      <div class="col-9 padding0 ml-auto">
                        <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>
                        <a href="javascript:void(0);" class="btn btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg" alt="">
                        </a>
                      </div>

                    </div>


                  </div>
                </div>

                <?php

                $i++;

              }
            }
            ?>
          </div>
        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   VEJA MAIS -->
    <!--  ==============================================================  -->

    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>
