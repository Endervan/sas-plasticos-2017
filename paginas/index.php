<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>



  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">
      <div id="container_banner">
        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   menu categoria -->
  <!--  ==============================================================  -->
  <div class="container-fluid menu_produtos_home">
    <div class="row">
      <div class="container ">
        <div class="row">
          <div class="col-3 top20"><h6>NOSSOS PRODUTOS</h6></div>
          <div class="col-9 mont">
            <div class="lista-produtos-index">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" id="myTab" role="tablist">

                <?php
                $result = $obj_site->select("tb_categorias_produtos"," limit 3");
                if (mysql_num_rows($result) > 0) {
                  $i = 0;
                  while($row = mysql_fetch_array($result)){
                    ?>

                    <li class="nav-item top25">
                      <a class="nav-item  <?php if ($i == 0) {echo 'active';} ?> nav-link" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>"  role="tab" aria-controls="<?php Util::imprime($row[titulo]); ?>" aria-expanded="true">
                        <?php Util::imprime($row[titulo]); ?>
                      </a>
                    </li>

                    <?php
                    $i++;
                  }
                }
                ?>
                <li class="nav-item ml-auto top25">
                  <a class="btn btn_todos " href="<?php echo Util::caminho_projeto() ?>/produtos/">
                    VER TODOS PRODUTOS
                  </a>
                </li>

              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   menu categoria -->
  <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!-- slider    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/lista_produtos_home.php') ?>
  <!-- ======================================================================= -->
  <!-- slider    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   EMPRESA E SERVICOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_empresa_servicos">
    <div class="row">

      <div class="container">
        <div class="row top80 mont">
          <?php
          $result = $obj_site->select("tb_servicos","order by rand() limit 2");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while($row = mysql_fetch_array($result)){
              if ($i == 0) {
                $margem = 'ml-auto';
                $i++;
              }else{
                $margem = '';
                $i = 0;
              }

              ?>

              <div class="col-5 <?php echo $margem; ?>">
                <div class="row">
                  <div class="col-12"><h3 class=" titulo_servicos_home text-uppercase"><span><?php Util::imprime($row[titulo]); ?></span></h3></div>
                  <div class="col-6 top5">
                    <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 214, 120, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                    </a>
                  </div>
                  <div class="col-6">
                    <div class=" desc_conj "><p><?php Util::imprime($row[descricao]); ?></p></div>
                    <div class="top20 text-right">
                      <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais_conj">MAIS DETALHES</a>
                    </div>
                  </div>
                </div>
              </div>

              <?php
              if ($i==0) {
                echo "<div class='clearfix'>  </div>";
              }else {
                $i++;
              }
            }
          }
          ?>

        </div>
      </div>

      <div class="container mont bottom10 top145">
        <div class="row">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
          <div class="col-6">
            <h4><?php Util::imprime($row[titulo]); ?></h4>
            <!-- <h4 class="text-uppercase"><span>embalagens plásticas i no embalo da vida </span></h4> -->
            <div class="desc_empresa_home top20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>

          <div class="col-6 top285 text-left">
            <a href="<?php echo Util::caminho_projeto() ?>/empresa" title="Saiba Mais" class="btn saiba_mais_conj">
              <div class="media">
                CONHEÇA MAIS <br> NOSSA EMPRESA
                <div class="media-body">
                  <i class="fa fa-arrow-circle-o-right fa-3x ml-3 mt-0 mb-1" aria-hidden="true"></i>
                </div>
              </div>
            </a>
          </div>

        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA E SERVICOS -->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!--   EMPRESA SUSTENTABILIDADE -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_sustentabilidade">
    <div class="row">

      <div class="container mont bottom10 top60">
        <div class="row">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
          <div class="col-6 bg_branco ml-auto">
            <h4><?php Util::imprime($row[titulo]); ?></h4>
            <h4><span>MEIO AMBIENTE</span></h4>
            <div class="desc_empresa_home top20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>

            <div class="col-12  padding0 top25 text-right">
              <a href="<?php echo Util::caminho_projeto() ?>/empresa" title="Saiba Mais" class="btn saiba_mais_emp">
                <div class="media">
                  MAIS DETALHES
                  <div class="media-body">
                    <i class="fa fa-arrow-circle-o-right fa-2x ml-3 mt-0 mb-1" aria-hidden="true"></i>
                  </div>
                </div>
              </a>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA SUSTENTABILIDADE -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">

(function() {

  // store the slider in a local variable
  var $window = $(window),
  flexslider;

  // tiny helper function to add breakpoints
  function getGridSize() {
    return (window.innerWidth < 600) ? 2 :
    (window.innerWidth < 900) ? 3 : 4;
  }

  $(function() {
    SyntaxHighlighter.all();
  });

  $window.load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationSpeed: 600,
      animationLoop: false,
      itemWidth: 100,
      itemMargin: 0,
      minItems: getGridSize(), // use function to pull in initial value
      maxItems: getGridSize(), // use function to pull in initial value
      start: function(slider){
        $('body').removeClass('loading');
        flexslider = slider;
      }
    });
  });

  // check grid size on resize event
  $window.resize(function() {
    var gridSize = getGridSize();

    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
  });
}());

</script>
