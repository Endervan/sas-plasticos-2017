-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Dez-2016 às 12:19
-- Versão do servidor: 5.6.30
-- PHP Version: 5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monalisa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_atuacoes`
--

CREATE TABLE IF NOT EXISTS `tb_atuacoes` (
  `idatuacao` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=154 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_atuacoes`
--

INSERT INTO `tb_atuacoes` (`idatuacao`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`, `cargo`, `telefone`, `email`, `facebook`) VALUES
(150, 'Indústria Energética', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-energética', NULL, NULL, NULL, NULL, '', '', '', NULL),
(145, 'Indústria de Exploração', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-de-exploração', NULL, NULL, NULL, NULL, '', '', '', NULL),
(146, 'Indústria de Extração', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-de-extração', NULL, NULL, NULL, NULL, '', '', '', NULL),
(147, 'Setor Automotivo', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'setor-automotivo', NULL, NULL, NULL, NULL, '', '', '', NULL),
(148, 'Indústria Alimentícia', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'indústria-alimentícia', NULL, NULL, NULL, NULL, '', '', '', NULL),
(149, 'Setor Agrícola', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'setor-agrícola', NULL, NULL, NULL, NULL, '', '', '', NULL),
(152, 'Infraestrutura', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'infraestrutura', NULL, NULL, NULL, NULL, '', '', '', NULL),
(153, 'Transporte Pesado', NULL, 'imagem_nao_disponivel.jpg', 'SIM', NULL, 'transporte-pesado', NULL, NULL, NULL, NULL, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_clientes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `imagem_clientes`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Index 01', '0312201603261334857869.jpg', NULL, 'SIM', NULL, '1', 'index-01', '', NULL, NULL),
(2, 'Index 02', '0312201603271162207891.jpg', NULL, 'SIM', NULL, '1', 'index-02', '/produtos', NULL, NULL),
(3, 'Banner 03', '0312201603281182927125.jpg', NULL, 'SIM', NULL, '1', 'banner-03', '/servicos', NULL, NULL),
(4, 'Mobile - 1', '0612201612391339652227.jpg', NULL, 'SIM', NULL, '2', 'mobile--1', '', NULL, NULL),
(5, 'Mobile - 2', '0612201612391310783535.jpg', NULL, 'SIM', NULL, '2', 'mobile--2', '', NULL, NULL),
(6, 'Mobile - 3', '0612201612401323985016.jpg', NULL, 'SIM', NULL, '2', 'mobile--3', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE IF NOT EXISTS `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '0512201611151342127207.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Serviços', '0512201612091238829642.jpg', NULL, 'servicos', 'SIM', NULL),
(3, 'Serviços Dentro', '0512201601141153691036.jpg', NULL, 'servicos-dentro', 'SIM', NULL),
(4, 'Dicas', '0512201602121213711867.jpg', NULL, 'dicas', 'SIM', NULL),
(5, 'Dicas Dentro', '0512201603561316559113.jpg', NULL, 'dicas-dentro', 'SIM', NULL),
(6, 'Fale Conosco', '0512201604421193255688.jpg', NULL, 'fale-conosco', 'SIM', NULL),
(7, 'Trabalhe Conosco', '0512201606521309465417.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(8, 'Produtos', '0512201607411326736108.jpg', NULL, 'produtos', 'SIM', NULL),
(9, 'Orçamento', '0612201612131148877750.jpg', NULL, 'orcamento', 'SIM', NULL),
(25, 'Mobile - Empresa', '0612201611411220704782.jpg', NULL, 'mobile--empresa', 'SIM', NULL),
(26, 'Mobile - Serviços', '0712201610441232583976.jpg', NULL, 'mobile--servicos', 'SIM', NULL),
(27, 'Mobile - Serviços dentro', '0712201611031120653704.jpg', NULL, 'mobile--servicos-dentro', 'SIM', NULL),
(28, 'Mobile - Dicas', '0712201611371259317695.jpg', NULL, 'mobile--dicas', 'SIM', NULL),
(29, 'Mobile - Dicas dentro', '0712201611371259317695.jpg', NULL, NULL, 'SIM', NULL),
(30, 'Mobile - Fale Conosco', NULL, NULL, NULL, 'SIM', NULL),
(31, 'Mobile - Trabalhe Conosco', NULL, NULL, NULL, 'SIM', NULL),
(32, 'Mobile - Produtos', NULL, NULL, NULL, 'SIM', NULL),
(33, 'Mobile - Orçamento', NULL, NULL, NULL, 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'JANELAS', NULL, '0412201612101172829795.png', 'SIM', NULL, 'janelas', '', '', ''),
(88, 'PORTAS', NULL, '0412201612111386903544.png', 'SIM', NULL, 'portas', '', '', ''),
(86, 'DECKS', NULL, '0412201612121332420583.png', 'SIM', NULL, 'decks', '', '', ''),
(87, 'CORRIMÃO', NULL, '0412201612121275801326.png', 'SIM', NULL, 'corrimao', '', '', ''),
(89, 'FACHADAS', NULL, '0412201612131181616322.png', 'SIM', NULL, 'fachadas', '', '', ''),
(90, 'CATEGORIA 1', NULL, '0412201612131158429559.png', 'SIM', NULL, 'categoria-1', '', '', ''),
(91, 'CATEGORIA 2', NULL, '0412201612131121747126.png', 'SIM', NULL, 'categoria-2', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_clientes`
--

CREATE TABLE IF NOT EXISTS `tb_clientes` (
  `idcliente` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_clientes`
--

INSERT INTO `tb_clientes` (`idcliente`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(26, 'IPE', NULL, '1410201611255884745133..jpg', 'SIM', NULL, 'ipe', NULL, NULL, NULL, ''),
(25, 'UNDERDOG', NULL, '1410201611246836184821..jpg', 'SIM', NULL, 'underdog', NULL, NULL, NULL, ''),
(24, 'VOGUE', NULL, '1410201611238885297795..jpg', 'SIM', NULL, 'vogue', NULL, NULL, NULL, ''),
(23, 'TERRAÇO NEW YORK', NULL, '1410201611228992290041..jpg', 'SIM', NULL, 'terraco-new-york', NULL, NULL, NULL, ''),
(27, 'EMPADA DO ALBERTO', NULL, '1410201611265590312369..jpg', 'SIM', NULL, 'empada-do-alberto', NULL, NULL, NULL, ''),
(28, 'PADRÃO', NULL, '1410201611275758642090..jpg', 'SIM', NULL, 'padrao', NULL, NULL, NULL, ''),
(29, 'TIOBAKINAS', NULL, '1410201611276442432275..jpg', 'SIM', NULL, 'tiobakinas', NULL, NULL, NULL, ''),
(30, 'ARENA', NULL, '1410201611282579654815..jpg', 'SIM', NULL, 'arena', NULL, NULL, NULL, ''),
(31, 'GENTLEMAN SEGURANÇA', NULL, '1410201611297026933344..jpg', 'NAO', NULL, 'gentleman-seguranca', NULL, NULL, NULL, ''),
(32, 'BIOLOGICA', NULL, '2510201604456540585834..jpg', 'SIM', NULL, 'biologica', '', '', '', ''),
(33, 'SELETTI', NULL, '2510201604476392277619..jpg', 'SIM', NULL, 'seletti', '', '', '', ''),
(34, 'CONFRADE', NULL, '2510201604473877080620..jpg', 'SIM', NULL, 'confrade', '', '', '', ''),
(35, 'ELF', NULL, '2510201604496199755411..jpg', 'SIM', NULL, 'elf', '', '', '', ''),
(36, 'MIRACRUZ', NULL, '2510201604493711471851..jpg', 'SIM', NULL, 'miracruz', '', '', '', ''),
(37, 'META CREO', NULL, '2510201604502209761506..jpg', 'SIM', NULL, 'meta-creo', '', '', '', ''),
(38, 'JARDIM E CIA', NULL, '2510201604501338859531..jpg', 'SIM', NULL, 'jardim-e-cia', '', '', '', ''),
(39, 'CASARIN', NULL, '2510201604502349868197..jpg', 'SIM', NULL, 'casarin', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `ddd1` varchar(10) NOT NULL,
  `ddd2` varchar(10) NOT NULL,
  `ddd3` varchar(10) NOT NULL,
  `ddd4` varchar(10) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `link_place` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`, `ddd1`, `ddd2`, `ddd3`, `ddd4`, `facebook`, `twitter`, `link_place`) VALUES
(1, '', '', '', 'SIM', 0, '', 'Rua 87 nº 553 St. Sul - Goiânia-GO', '3996-5500', '3322-5000', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d30576.35403215722!2d-49.28782860624648!3d-16.67466878073772!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef14fb6786dcb%3A0xad9753dc82de20f0!2sInove+Uniformes!5e0!3m2!1spt-BR!2sbr!4v1480136801834', NULL, NULL, 'marciomas@gmail.com, junior@homewebbrasil.com.br', 'https://plus.google.com/110190181962511972543', '', '', '(62)', '(65)', '', '', 'https://www.facebook.com/inoveuniformeslojas/', '', 'https://www.google.com/maps/place/Inove+Uniformes/@-16.689694,-49.276219,13z/data=!4m5!3m4!1s0x0:0xad9753dc82de20f0!8m2!3d-16.6896937!4d-49.2762191?hl=pt-BR');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(1, 'Dicas de uniformes para sua empresa', '<p>\r\n	O estilo do uniforme pode determinar o comportamento no ambiente corporativo. Atualmente, com o &ldquo;dress code&rdquo;, as empresas passaram a realizar palestras e a contratar consultores de imagem para orientar os colaboradores a se vestir adequadamente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	As empresas que adotam uniformes, e at&eacute; as empresas que criam camisetas como uniforme, as camisetas personalizadas com o logo da empresa, passam sinal de profissionalismo, *fortalecem a sua *marca, pois transmitem a* imagem da empresa*.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O profissional quando se apresenta para algum cliente ou visita alguma organiza&ccedil;&atilde;o devidamente uniformizado, passa a imagem de uma empresa organizada, segura, s&eacute;ria, enfim, transmite confian&ccedil;a, podendo representar um diferencial no atendimento ao cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O uso do uniforme evita coment&aacute;rios entre os pr&oacute;prios colaboradores, onde uma pessoa pode chamar mais a aten&ccedil;&atilde;o que outras, pelo uso de uma roupa mais decotada ou curta, pelo excesso de acess&oacute;rios ou uso de maquiagem inadequada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&Eacute; importante lembrar que os profissionais s&atilde;o o* cart&atilde;o de visita* da empresa, podendo causar boa ou m&aacute; impress&atilde;o ao cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Se optar pelo uso de uniforme ou camisetas personalizadas no seu neg&oacute;cio, lembre-se que &eacute; necess&aacute;rio estabelecer uma pol&iacute;tica sobre o devido uso, conscientizando todos os colaboradores da empresa. Deixe claro de quem &eacute; a responsabilidade pela lavagem e conserva&ccedil;&atilde;o das pe&ccedil;as, e lembre-se de pedir para o colaborador assinar um termo de ci&ecirc;ncia sobre as regras.&nbsp;</p>\r\n<p>\r\n	&nbsp;&nbsp;</p>\r\n<p>\r\n	Defina as pe&ccedil;as que comp&otilde;em o uniforme e explique aos colaboradores que as pe&ccedil;as dever&atilde;o ser mantidas com suas caracter&iacute;sticas originais, ou seja, n&atilde;o sendo permitida a customiza&ccedil;&atilde;o das pe&ccedil;as para n&atilde;o descaracterizar a padroniza&ccedil;&atilde;o e o modelo do uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sugira cal&ccedil;as, saias, blusas, camisetas, camisas polo, enfim, pe&ccedil;as cl&aacute;ssicas como uniforme com o* logo da empresa* e com cores s&oacute;brias que identifiquem a organiza&ccedil;&atilde;o. J&aacute; prepare pe&ccedil;as para ver&atilde;o, inverno e meia esta&ccedil;&atilde;o. Os sapatos dever&atilde;o ser fechados, garantindo a eleg&acirc;ncia e discri&ccedil;&atilde;o. Adote cores s&oacute;brias de f&aacute;cil combina&ccedil;&atilde;o. Tr&ecirc;s unidades de cada pe&ccedil;a ser&atilde;o suficientes para o dia a dia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para minimizar custos, uma simples camiseta devidamente personalizada com a marca*da empresa, &eacute; suficiente para *identificar e divulgar a empresa.</p>\r\n<div>\r\n	&nbsp;</div>', '0512201609551329934314..jpg', 'SIM', NULL, 'dicas-de-uniformes-para-sua-empresa', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(2, 'O que você deve saber na hora da comprar seus uniformes', '<p>\r\n	- Cuidado com a sua numera&ccedil;&atilde;o ou com a numera&ccedil;&atilde;o de sua equipe. A sua medida pode variar de confec&ccedil;&atilde;o para confec&ccedil;&atilde;o. D&ecirc; prefer&ecirc;ncia &agrave;s empresas que trabalham com o padr&atilde;o de numera&ccedil;&atilde;o da ABNT (Associa&ccedil;&atilde;o Brasileira de Normas T&eacute;cnicas).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Personalize a pe&ccedil;a superior do seu uniforme com o logotipo da sua empresa. Se voc&ecirc; optou por cal&ccedil;a social e camisa social, personalize a camisa social com o logotipo da sua empresa. Se voc&ecirc; optou por cal&ccedil;a, camisa e blazer, &eacute; recomend&aacute;vel que voc&ecirc; personalize a camisa e o blazer. Se voc&ecirc; optar somente por uma pe&ccedil;a, d&ecirc; prefer&ecirc;ncia pela pe&ccedil;a superior, como camiseta ou um camisete, uma blusinha, enfim, pois o seu logotipo ficar&aacute; mais vis&iacute;vel nas pe&ccedil;as superiores. Algumas empresas personalizam o logotipo em serigrafia (silkscreen) e bordado. D&ecirc; prefer&ecirc;ncia pelo bordado, devido &agrave; alta qualidade. O silkscreen &eacute; indicado para camisetas promocionais ou operacionais, devido &agrave; durabilidade.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Conhe&ccedil;a o tecido que est&aacute; comprando. Os tecidos de algod&atilde;o s&atilde;o confort&aacute;veis e facilitam a respira&ccedil;&atilde;o do corpo, mas s&atilde;o f&aacute;ceis de amassar. Os de tecido sint&eacute;tico possuem uma est&eacute;tica e caimento melhor, amassam menos, por&eacute;m, superaquecem o corpo. O recomend&aacute;vel &eacute; que o tecido para camisas seja misto (com algod&atilde;o e tecido sint&eacute;tico) e para pe&ccedil;as inferiores (como cal&ccedil;a, saia, etc...) seja de um tecido 100% poli&eacute;ster.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Se a quantidade for alta, compre direto da f&aacute;brica. Algumas ind&uacute;strias conseguem atender somente encomendas altas por se tratar de fabrica&ccedil;&atilde;o pr&oacute;pria. Entretanto, &eacute; poss&iacute;vel comprar de confec&ccedil;&otilde;es pequenas. A vantagem de comprar direto da f&aacute;brica &eacute; que voc&ecirc; ter&aacute; um produto de qualidade a um pre&ccedil;o extremamente acess&iacute;vel, por&eacute;m, para obter o m&aacute;ximo dos benef&iacute;cios voc&ecirc; precisa disponibilizar um tempo para a produ&ccedil;&atilde;o. O prazo de confec&ccedil;&atilde;o varia de 30 a 45 dias dependendo do porte da empresa. &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Se for comprar direto da f&aacute;brica, compre um pequeno estoque para deixar de reserva. Geralmente, as empresas possuem um m&iacute;nimo de pe&ccedil;as para produ&ccedil;&atilde;o. &Eacute; recomend&aacute;vel que voc&ecirc; disponibilize 10% do seu pedido em estoque para eventuais casos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por exemplo, se a sua empresa precisa de 100 (cem) camisetas, compre 110 (cento e dez), para deixar 10 (dez) pe&ccedil;as em estoque. A empresa que voc&ecirc; encomendou as 100 (cem) camisetas n&atilde;o far&aacute; o mesmo pre&ccedil;o para apenas as 10 (dez) que voc&ecirc; comprou posteriormente. Ent&atilde;o, compre sempre 10% a mais do que precisa, mesmo porque, se entrar um novo funcion&aacute;rio, voc&ecirc; n&atilde;o poder&aacute; deix&aacute;-lo sem uniforme at&eacute; a troca do uniforme de toda a equipe, n&atilde;o &eacute; mesmo?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Exija garantia. As grandes empresas oferecem garantia de 1 a 2 anos contra defeito de fabrica&ccedil;&atilde;o.&nbsp;</p>\r\n<p>\r\n	Essa garantia &eacute; importante pois, se voc&ecirc; comprou uniformes com alguma empresa e deixou algumas pe&ccedil;as em estoque, e alguns meses depois entrou um novo funcion&aacute;rio e esse funcion&aacute;rio viu defeito na fabrica&ccedil;&atilde;o da pe&ccedil;a, &eacute; necess&aacute;rio que a empresa efetue a troca ou reforma do mesmo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Confira os modelos que mais agradaram. N&atilde;o basta escolher o melhor tecido, mas tamb&eacute;m &eacute; necess&aacute;rio verificar se o modelo escolhido agrada voc&ecirc;, tanto esteticamente quanto no conforto di&aacute;rio. Uniforme &eacute; algo que ser&aacute; utilizado por voc&ecirc; pelo menos 8 horas por dia e 5 dias por semana. A qualidade do tecido &eacute; fundamental, a est&eacute;tica tamb&eacute;m, por&eacute;m, o conforto &eacute; um dos fatores mais decisivos na hora de escolher seu uniforme.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Com essas informa&ccedil;&otilde;es voc&ecirc; j&aacute; sabe o que fazer na hora de comprar seus uniformes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Boa sorte!</p>', '0512201609551117639639..jpg', 'SIM', NULL, 'o-que-voce-deve-saber-na-hora-da-comprar-seus-uniformes', '', '', '', NULL),
(44, 'A imagem é um fator preponderante', '<p style="text-align: justify;">\r\n	A constru&ccedil;&atilde;o de uma boa imagem tem como base ser e parecer e para tanto a aten&ccedil;&atilde;o e vigil&acirc;ncia tornam-se fatores essenciais que devem ser considerados no intuito de impedir ou evitar qualquer deslize fatal. Cria-la de forma s&oacute;lida requer observa&ccedil;&atilde;o e estudo, pois s&oacute; se tornar&aacute; real e causar&aacute; efeito positivo se for verdadeira.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O uniforme corporativo &eacute; uma forte ferramenta para implantar e aprimorar a cultura da eleg&acirc;ncia e da imagem adequada dentro de uma empresa. As organiza&ccedil;&otilde;es que fazem uso deste acess&oacute;rio como forma de padroniza&ccedil;&atilde;o adequada a um ambiente profissional d&atilde;o um passo muito importante para a constru&ccedil;&atilde;o de sua boa imagem, por&eacute;m esta quest&atilde;o envolve mais que o uso em si de roupas iguais. A constru&ccedil;&atilde;o de uma imagem profissional reconhecida pelo mercado como exemplar e de sucesso n&atilde;o parte da base da pir&acirc;mide, o papel dos dirigentes &eacute; fundamental porque estes se tornam refer&ecirc;ncias para seus colaboradores e conseq&uuml;entemente todos se tornam o reflexo de sua postura. Os funcion&aacute;rios tomam de exemplo seus superiores hier&aacute;rquicos e sentem-se incentivados a se portarem da mesma maneira. Este comportamento &eacute; recebido e percebido pelos clientes externos e esta imagem disseminada no mercado. As empresas que fazem uso do uniforme aliado a pr&aacute;ticas de etiqueta profissional conquistam uma imagem positiva e esta repercute no mercado favorecendo seus neg&oacute;cios, por&eacute;m esta postura dever&aacute; ser constantemente orientada e reconhecida para seu aprimoramento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Os benef&iacute;cios que o uso do uniforme traz s&atilde;o diversos e v&atilde;o desde o conforto e economia de tempo e dinheiro para o funcion&aacute;rio &agrave; adequa&ccedil;&atilde;o do ambiente de trabalho que reflete bom gosto e respeito pelos clientes. Quando se refere ao mercado de trabalho a boa imagem pode ser em muitos casos um fator decisivo, portanto torna-se responsabilidade de cada um e corresponder&aacute; a maneira como o indiv&iacute;duo deseja ser percebido profissionalmente.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Desenvolver e trabalhar a conscientiza&ccedil;&atilde;o dos usu&aacute;rios de vestu&aacute;rio profissional sobre a import&acirc;ncia do uso do uniforme, da manuten&ccedil;&atilde;o adequada para prolongar a vida &uacute;til das pe&ccedil;as &eacute; fundamental. Proporcionar aos seus funcion&aacute;rios acesso f&aacute;cil e orienta&ccedil;&atilde;o constante sobre os benef&iacute;cios do uso da etiqueta, da boa postura e comportamento profissional, automaquilagem, cuidados com a pele, com os cabelos, escolha adequada dos acess&oacute;rios (bolsa, sapatos, j&oacute;ias e bjouterias) auxiliar&aacute; na redu&ccedil;&atilde;o da resist&ecirc;ncia, muitas vezes criada pelos funcion&aacute;rios sobre uso do uniforme. Inserir e disseminar a import&acirc;ncia do papel fundamental de cada colaborador para a boa imagem da empresa e conscientizar que a op&ccedil;&atilde;o pelo uso do uniforme &eacute; investimento que beneficia a empresa e principalmente os funcion&aacute;rios. Estes usufruem oportunidades de desenvolvimento pessoal e social, fator essencial para manter a alta estima e a motiva&ccedil;&atilde;o. Cuidar de seu uniforme representa mais que manter uma pe&ccedil;a de vestu&aacute;rio em bom estado &eacute; a tradu&ccedil;&atilde;o de sua apresenta&ccedil;&atilde;o pessoal e isto possui grande repercuss&atilde;o na ascens&atilde;o e sucesso de sua carreira profissional.</p>', '0512201609561161389182..jpg', 'SIM', NULL, 'a-imagem-e-um-fator-preponderante', '', '', '', NULL),
(45, 'importância dos uniformes profissionais para a segurança do trabalho', '<p style="text-align: justify;">\r\n	Respons&aacute;veis pela identifica&ccedil;&atilde;o dos funcion&aacute;rios de uma empresa e por manter uma padroniza&ccedil;&atilde;o dentro do ambiente de trabalho, os uniformes profissionais s&atilde;o obrigat&oacute;rios em alguns ramos de atua&ccedil;&atilde;o. Funcion&aacute;rios da constru&ccedil;&atilde;o civil, profissionais que trabalham em rodovias, em cozinhas industriais, com limpeza de hot&eacute;is, cientistas laboratoriais, policiais, bombeiros, gar&ccedil;ons, recepcionistas e eletricistas s&atilde;o s&oacute; alguns exemplos de profissionais que atuam uniformizados.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Al&eacute;m de identificar um funcion&aacute;rio para o p&uacute;blico, ou apontar sua fun&ccedil;&atilde;o dentro de um ambiente industrial ou comercial, os uniformes profissionais tamb&eacute;m s&atilde;o importantes para a seguran&ccedil;a do trabalho. O uso se faz necess&aacute;rio principalmente em atividades em que a sa&uacute;de e integridade do profissional podem ser colocadas em risco. Um bom exemplo &eacute; o corpo de bombeiros, cujo uniforme funciona como protetor t&eacute;rmico e tamb&eacute;m contra as chamas de um inc&ecirc;ndio.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Para eletricistas e funcion&aacute;rios da constru&ccedil;&atilde;o civil, o uso de uniformes profissionais garante muito mais seguran&ccedil;a em obras de pequeno, m&eacute;dio e grande porte. O traje adequado para estas duas profiss&otilde;es &eacute; fabricado com material &nbsp;que protege a pele contra descargas el&eacute;tricas, corros&otilde;es, materiais pontiagudos e at&eacute; mesmo contra raios UV. Esses uniformes profissionais s&atilde;o t&atilde;o importantes como equipamentos de prote&ccedil;&atilde;o como &oacute;culos, luvas e protetores auriculares.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Ainda h&aacute; outras situa&ccedil;&otilde;es em que o uso de uniformes profissionais &eacute; de vital import&acirc;ncia, como para funcion&aacute;rios que trabalham em c&acirc;meras frigor&iacute;ficas. Estes precisam de trajes resistentes a baixas temperaturas e que mantenham a condi&ccedil;&atilde;o f&iacute;sica do profissional intacta, mesmo em ambientes extremamente frios. Sem os trajes adequados nestas situa&ccedil;&otilde;es, o desempenho de simples atividades seria imposs&iacute;vel e prejudicaria a cadeia produtiva da empresa.</p>', '0512201609561297983096..jpg', 'SIM', NULL, 'importancia-dos-uniformes-profissionais-para-a-seguranca-do-trabalho', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Home - Descrição Empresa', '<p>\r\n	Uma das mais conceituadas e tradicionais empresas de Esquadrias em madeira do mercado Goiano, a Monaliz<br />\r\n	a Esquadrias em Madeira, fundada em 1990 e com sede na Rua 87 no Setor sul - Goi&acirc;nia, trabalha exclusivam<br />\r\n	ente com pe&ccedil;as sob encomenda e design personalizados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Utilizamos as melhores ferragens do mercado brasileiro, sendo constante o contato com os fornecedores no intuito de verificar lan&ccedil;amentos e agreg&aacute;-los aos nossos produtos. Oferecemos aos nossos clientes solu&ccedil;&otilde;es funcionais, inovadoras e pr&aacute;ticas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Com industria pr&oacute;pria, a madeira passa por processo natural de secagem, permitindo assim que o produto final tenha garantia de vida longa e estabilidade. Nosso trabalho vai desde a tomada de medidas, execu&ccedil;&atilde;o de projetos e acessoria ao cliente durante todo o processo de fabrica&ccedil;&atilde;o.</p>', 'SIM', 0, '', '', '', 'home--descricao-empresa', NULL, NULL, NULL),
(2, 'Contato - Texto', '<p>\r\n	Entre em contato conosco.</p>\r\n<p>\r\n	Envie suas sugest&otilde;es, elogios ou cr&iacute;ticas.</p>', 'SIM', 0, '', '', '', 'contato--texto', NULL, NULL, NULL),
(3, 'Empresa', '<p>\r\n	TEXTO DA EMPRESA</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma das mais conceituadas e tradicionais empresas de Esquadrias em madeira do mercado Goiano, a Monaliz a Esquadrias em Madeira, fundada em 1990 e com sede na Rua 87 no Setor sul - Goi&acirc;nia, trabalha exclusivam ente com pe&ccedil;as sob encomenda e design personalizados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Utilizamos as melhores ferragens do mercado brasileiro, sendo constante o contato com os fornecedores no intuito de verificar lan&ccedil;amentos e agreg&aacute;-los aos nossos produtos. Oferecemos aos nossos clientes solu&ccedil;&otilde;es funcionais, inovadoras e pr&aacute;ticas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Com industria pr&oacute;pria, a madeira passa por processo natural de secagem, permitindo assim que o produto final tenha garantia de vida longa e estabilidade. Nosso trabalho vai desde a tomada de medidas, execu&ccedil;&atilde;o de projetos e acessoria ao cliente durante todo o processo de fabrica&ccedil;&atilde;o.</p>', 'SIM', 0, '', '', '', 'empresa', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipes`
--

CREATE TABLE IF NOT EXISTS `tb_equipes` (
  `idequipe` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE IF NOT EXISTS `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_facebook`
--

CREATE TABLE IF NOT EXISTS `tb_facebook` (
  `idface` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_facebook`
--

INSERT INTO `tb_facebook` (`idface`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'face 1 Lorem ipsum dolor sit amet', '<p>\r\n	teste 01 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'face-1-lorem-ipsum-dolor-sit-amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'face 2 Lorem ipsum dolor sit amet', '<p>\r\n	teste 2 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</p>\r\n<p>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.<br />\r\n	&nbsp;<br />\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</p>\r\n<p>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</p>\r\n<p>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'face 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste 03&nbsp; Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'face 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste 04 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS&nbsp; com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'face 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 5 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.<br />\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'face 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	teste 06 Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'face 7 Lorem ipsum dolor sit amet', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '2107201607121394938875..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias`
--

CREATE TABLE IF NOT EXISTS `tb_galerias` (
  `idgaleria` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias`
--

INSERT INTO `tb_galerias` (`idgaleria`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(36, 'BARES E RESTAURANTES', '1110201603492601083566.png', 'SIM', NULL, 'bares-e-restaurantes', NULL),
(37, 'LIMPEZA EM GERAL', '2409201612231228099970.png', 'SIM', NULL, 'limpeza-em-geral', NULL),
(38, 'ESCRITÓRIO E COMÉRCIO', '1110201603472583030532.png', 'SIM', NULL, 'escritorio-e-comercio', NULL),
(43, 'INDÚSTRIA E CONSTRUÇÃO CIVIL', '1110201603491738726090.png', 'SIM', NULL, 'industria-e-construcao-civil', NULL),
(44, 'ÁREA DE SAÚDE', '1110201603483164367804.png', 'SIM', NULL, 'area-de-saude', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_geral`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_geral` (
  `id_galeriageral` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_galeria` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_geral`
--

INSERT INTO `tb_galerias_geral` (`id_galeriageral`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_galeria`) VALUES
(126, '1110201606129370584777.jpg', 'SIM', NULL, NULL, 36),
(127, '1110201606128491200018.jpg', 'SIM', NULL, NULL, 36),
(128, '1110201606123935418981.jpg', 'SIM', NULL, NULL, 36),
(129, '1110201606125431446035.jpg', 'SIM', NULL, NULL, 36),
(130, '1110201606121990333273.jpg', 'SIM', NULL, NULL, 36),
(131, '1110201606129567316079.jpg', 'SIM', NULL, NULL, 36),
(132, '1110201606123052659552.jpg', 'SIM', NULL, NULL, 36),
(133, '1110201606128704088876.jpg', 'SIM', NULL, NULL, 36),
(134, '1110201606121395695730.jpg', 'SIM', NULL, NULL, 36),
(135, '1110201606129286724370.jpg', 'SIM', NULL, NULL, 36),
(136, '1110201606122500877558.jpg', 'SIM', NULL, NULL, 36),
(137, '1110201606127282586809.jpg', 'SIM', NULL, NULL, 36),
(138, '1110201606167336113651.jpg', 'SIM', NULL, NULL, 37),
(139, '1110201606167126144290.jpg', 'SIM', NULL, NULL, 37),
(140, '1110201606166641264855.jpg', 'SIM', NULL, NULL, 37),
(141, '1110201606166856153934.jpg', 'SIM', NULL, NULL, 37),
(142, '1110201606198216783127.jpg', 'SIM', NULL, NULL, 38),
(143, '1110201606194614951705.jpg', 'SIM', NULL, NULL, 38),
(144, '1110201606194581049274.jpg', 'SIM', NULL, NULL, 38),
(145, '1110201606196961685398.jpg', 'SIM', NULL, NULL, 38),
(146, '1110201606192095600320.jpg', 'SIM', NULL, NULL, 38),
(147, '1110201606236245474128.jpg', 'SIM', NULL, NULL, 43),
(148, '1110201606238815510364.jpg', 'SIM', NULL, NULL, 43),
(149, '1110201606237030607693.jpg', 'SIM', NULL, NULL, 43),
(150, '1110201606236153364814.jpg', 'SIM', NULL, NULL, 43),
(151, '1110201606235868531324.jpg', 'SIM', NULL, NULL, 43),
(152, '1110201606278714095320.jpg', 'SIM', NULL, NULL, 44),
(153, '1110201606278558400455.jpg', 'SIM', NULL, NULL, 44),
(154, '1110201606276515941147.jpg', 'SIM', NULL, NULL, 44),
(155, '1110201606272921856492.jpg', 'SIM', NULL, NULL, 44),
(156, '1110201606278393876192.jpg', 'SIM', NULL, NULL, 44),
(157, '1110201606279452905394.jpg', 'SIM', NULL, NULL, 44),
(158, '1110201606279434882745.jpg', 'SIM', NULL, NULL, 44);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(106, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 9),
(107, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 9),
(108, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 10),
(109, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 10),
(110, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 11),
(111, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 11),
(112, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 12),
(113, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 12),
(114, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 13),
(115, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 13),
(116, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 14),
(117, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 14),
(118, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 15),
(119, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 15),
(120, '0312201603261334857869.jpg', 'SIM', NULL, NULL, 16),
(161, '0312201603271162207891.jpg', 'SIM', NULL, NULL, 16);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_servicos` (
  `id_galeriaservico` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_servico` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_uniformes`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_uniformes` (
  `id_galeriauniformes` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_uniforme` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_uniformes`
--

INSERT INTO `tb_galerias_uniformes` (`id_galeriauniformes`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_uniforme`) VALUES
(111, '2809201612081274866319.jpg', 'SIM', NULL, NULL, 1),
(112, '2809201612081336658844.jpg', 'SIM', NULL, NULL, 1),
(113, '2809201612081284847276.jpg', 'SIM', NULL, NULL, 1),
(114, '2809201612081126000436.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galeria_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `titulo`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(2, 'EMPRESA GALERIA 02', '2710201602115670501920.jpg', 'SIM', NULL, 'empresa-galeria-02', NULL),
(21, 'EMPRESA GALERIA 03', '2710201602114947246942.jpg', 'SIM', NULL, 'empresa-galeria-03', NULL),
(22, 'EMPRESA GALERIA 04', '2710201602115671943347.jpg', 'SIM', NULL, 'empresa-galeria-04', NULL),
(23, 'IMAGEM 4', '2710201603506180535070.jpg', 'SIM', NULL, 'imagem-4', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL,
  `super_admin` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`, `super_admin`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL, 'NAO'),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre', 'SIM'),
(3, 'Amanda', 'b362cb319b2e525dc715702edf416f10', 'SIM', 0, 'homewebbrasil@gmail.com', 'SIM', NULL, 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1292 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''1''', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''1''', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''2''', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''2''', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''4''', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''4''', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''NAO'' WHERE idproduto = ''6''', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = ''SIM'' WHERE idproduto = ''6''', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''43''', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''43''', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''44''', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''44''', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''46''', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''45''', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''46''', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = ''SIM'' WHERE iddica = ''45''', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''3''', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''NAO'' WHERE idlogin = ''2''', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = ''SIM'' WHERE idlogin = ''2''', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''4''', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''5''', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = ''6''', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'', id_grupologin = '''' WHERE idlogin = ''2''', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = '''', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = ''2'', email = ''2'' WHERE idlogin = ''2''', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''4''', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''5''', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = ''1''', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''70''', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''73''', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''71''', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''72''', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''81''', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''SIM'' WHERE idcategoriaproduto = ''81''', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = ''NAO'' WHERE idnoticia = ''46''', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = ''NAO'' WHERE idnoticia = ''45''', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = ''45''', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = ''SIM'' WHERE idnoticia = ''46''', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''80''', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''81''', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''82''', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''80''', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''81''', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''82''', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = ''2''', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''5''', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''40''', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''42''', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''43''', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''44''', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''8''', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''9''', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''10''', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''11''', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''12''', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''13''', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''14''', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''41''', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''46''', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1),
(489, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:35:23', 1),
(490, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-03', '14:48:13', 1),
(491, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:16', 1),
(492, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:22:59', 1),
(493, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:27:37', 1),
(494, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:28:23', 1),
(495, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '12:33:30', 1),
(496, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:20', 1),
(497, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:01:27', 1),
(498, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '13:04:09', 1),
(499, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '13:12:53', 1),
(500, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:08:56', 1),
(501, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:10:32', 1),
(502, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:19:38', 1),
(503, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''5''', '2016-06-07', '14:20:12', 1),
(504, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''3''', '2016-06-07', '14:20:29', 1),
(505, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''4''', '2016-06-07', '14:20:31', 1),
(506, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:25:43', 1),
(507, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:26:57', 1),
(508, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '14:31:49', 1),
(509, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '14:58:26', 1),
(510, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:01:49', 1),
(511, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-06-07', '15:02:31', 1),
(512, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:02:44', 1),
(513, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:06:22', 1),
(514, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-06-07', '15:06:29', 1),
(515, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-06-07', '15:07:17', 1),
(516, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '15:08:05', 1),
(517, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''2''', '2016-06-07', '19:19:12', 1),
(518, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '19:20:53', 1),
(519, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-06-07', '16:48:33', 1),
(520, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-07', '16:57:35', 1),
(521, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:13', 1),
(522, 'CADASTRO DO CLIENTE ', '', '2016-06-07', '17:11:59', 1),
(523, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:05:10', 3),
(524, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '14:08:08', 3),
(525, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '12:30:55', 1),
(526, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '15:38:33', 1),
(527, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '18:39:40', 3),
(528, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-27', '20:42:20', 3),
(529, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:40:49', 1),
(530, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:41:59', 1),
(531, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:43:56', 1),
(532, 'CADASTRO DO CLIENTE ', '', '2016-06-27', '19:47:20', 1),
(533, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:07:52', 1),
(534, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:09:32', 1),
(535, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:10:41', 1),
(536, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:05', 1),
(537, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:14:39', 1),
(538, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:15:18', 1),
(539, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:16:56', 1),
(540, 'CADASTRO DO CLIENTE ', '', '2016-06-28', '17:17:48', 1),
(541, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:54:16', 1),
(542, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '17:55:13', 1),
(543, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:37:40', 1),
(544, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:43:05', 1),
(545, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-28', '19:46:24', 1),
(546, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '09:11:11', 1),
(547, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '11:34:48', 1),
(548, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '12:03:24', 1),
(549, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:02:56', 0),
(550, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:39:29', 0),
(551, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '19:40:31', 0),
(552, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:05:34', 0),
(553, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:13:26', 0),
(554, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '20:15:05', 0),
(555, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '21:31:04', 0),
(556, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:12:42', 1),
(557, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:23', 1),
(558, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:13:54', 1),
(559, 'CADASTRO DO CLIENTE ', '', '2016-06-29', '19:14:21', 1),
(560, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:09:27', 0),
(561, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:10:20', 0),
(562, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-29', '23:15:20', 0),
(563, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '01:11:29', 0),
(564, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:35:50', 1),
(565, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:55:00', 0),
(566, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '09:55:01', 1),
(567, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:59:56', 0),
(568, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:49:36', 1),
(569, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '10:50:07', 1),
(570, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '11:03:55', 1),
(571, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '12:52:10', 1),
(572, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '13:19:01', 1),
(573, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:00:30', 1),
(574, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-30', '17:41:42', 1),
(575, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:30:13', 1),
(576, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-01', '09:32:16', 1),
(577, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-06', '17:07:15', 1),
(578, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:50:58', 1),
(579, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:10', 1),
(580, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:22', 1),
(581, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-11', '08:51:30', 1),
(582, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '13:56:40', 1),
(583, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '14:02:37', 1),
(584, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-12', '17:20:46', 1),
(585, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-13', '21:33:07', 1),
(586, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '10:53:24', 1),
(587, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:37:57', 1),
(588, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:42:35', 1),
(589, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:46:29', 1),
(590, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '12:51:04', 1),
(591, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:24:55', 1),
(592, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:27:05', 1),
(593, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:28:29', 1),
(594, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:02', 1),
(595, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:29', 1),
(596, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:39', 1),
(597, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:33:48', 1),
(598, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:40:05', 1),
(599, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '15:47:41', 1),
(600, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:53:52', 1),
(601, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:42', 1),
(602, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:57:59', 1),
(603, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:13', 1),
(604, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:26', 1),
(605, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:41', 1),
(606, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:58:53', 1),
(607, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '15:59:08', 1),
(608, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '16:32:46', 1),
(609, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '18:56:30', 1),
(610, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:12', 1),
(611, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:31', 1),
(612, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:13:48', 1),
(613, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:00', 1),
(614, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:22', 1),
(615, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:14:46', 1),
(616, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:15:06', 1),
(617, 'CADASTRO DO CLIENTE ', '', '2016-07-14', '19:17:31', 1),
(618, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-14', '19:30:31', 1),
(619, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '08:52:29', 1),
(620, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '11:58:06', 1),
(621, 'CADASTRO DO CLIENTE ', '', '2016-07-15', '19:46:29', 1),
(622, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '19:47:35', 1),
(623, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''14''', '2016-07-15', '19:59:25', 1),
(624, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''13''', '2016-07-15', '19:59:32', 1),
(625, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''15''', '2016-07-15', '19:59:36', 1),
(626, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''16''', '2016-07-15', '19:59:42', 1),
(627, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''18''', '2016-07-15', '19:59:45', 1),
(628, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''17''', '2016-07-15', '19:59:49', 1),
(629, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-15', '20:14:55', 1),
(630, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '11:00:46', 1),
(631, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:28:19', 1),
(632, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:37:56', 1),
(633, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-16', '13:38:07', 1),
(634, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:05', 1),
(635, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:48:53', 1),
(636, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:11', 1),
(637, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:27', 1),
(638, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:49:45', 1),
(639, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:06', 1),
(640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:29', 1),
(641, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '09:50:58', 1),
(642, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:02:46', 1),
(643, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:04:37', 1),
(644, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:12:13', 1),
(645, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '10:15:36', 1),
(646, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:06:46', 1),
(647, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:49:01', 1),
(648, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:54:14', 1),
(649, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '17:55:10', 1),
(650, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:00:07', 1),
(651, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:08:04', 1),
(652, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:15:21', 1),
(653, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:16:03', 1),
(654, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:19:17', 1),
(655, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:22:55', 1),
(656, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-18', '18:25:29', 1),
(657, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '09:35:26', 1),
(658, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-20', '18:33:00', 1),
(659, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '08:47:49', 1),
(660, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:01:41', 1),
(661, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:30:24', 1),
(662, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '09:46:31', 1),
(663, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:41:53', 1),
(664, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '10:42:27', 1),
(665, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:01:08', 1),
(666, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '12:59:31', 1),
(667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '13:07:58', 1),
(668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-21', '19:12:55', 1),
(669, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_facebook WHERE idface = ''43''', '2016-07-21', '19:23:48', 1),
(670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:05', 1),
(671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:28', 1),
(672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:33:48', 1),
(673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:08', 1),
(674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:26', 1),
(675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-22', '10:34:41', 1),
(676, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:28', 1),
(677, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:36', 1),
(678, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:46', 1),
(679, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '11:08:57', 1),
(680, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:34:45', 1),
(681, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:57:51', 1),
(682, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '16:59:26', 1),
(683, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:01:05', 1),
(684, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:38', 1),
(685, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:04:55', 1),
(686, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:09', 1),
(687, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:26', 1),
(688, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:05:45', 1),
(689, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '17:06:02', 1),
(690, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-26', '18:07:45', 1),
(691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:29:36', 1),
(692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:32:50', 1),
(693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:33:06', 1),
(694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:14', 1);
INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(695, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-27', '09:34:39', 1),
(696, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-29', '08:57:37', 1),
(697, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '12:23:41', 1),
(698, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '15:32:54', 1),
(699, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:54:41', 1),
(700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '17:55:28', 1),
(701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '18:42:16', 1),
(702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '19:42:53', 1),
(703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:04:57', 1),
(704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-30', '20:12:02', 1),
(705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-07-31', '14:29:17', 1),
(706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:28', 1),
(707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:42', 1),
(708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:51:56', 1),
(709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '08:52:43', 1),
(710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:15:31', 1),
(711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '10:21:54', 1),
(712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '12:04:07', 1),
(713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '15:58:27', 1),
(714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '16:02:46', 1),
(715, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-01', '17:35:49', 1),
(716, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:56:30', 1),
(717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:01', 1),
(718, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:12', 1),
(719, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '10:57:23', 1),
(720, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '16:28:16', 1),
(721, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '17:20:26', 1),
(722, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '18:37:39', 1),
(723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:25:58', 1),
(724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:26:08', 1),
(725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:36', 1),
(726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-02', '19:58:47', 1),
(727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '09:09:03', 1),
(728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '10:12:40', 1),
(729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:44:59', 1),
(730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '16:52:14', 1),
(731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:12:56', 1),
(732, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-03', '17:47:19', 1),
(733, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-04', '20:15:30', 1),
(734, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:29', 1),
(735, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:27:38', 1),
(736, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: teste1@gmail.com', 'DELETE FROM tb_equipes WHERE idequipe = ''1''', '2016-08-25', '11:30:04', 1),
(737, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''2''', '2016-08-25', '11:30:06', 1),
(738, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''3''', '2016-08-25', '11:30:08', 1),
(739, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''4''', '2016-08-25', '11:30:09', 1),
(740, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''5''', '2016-08-25', '11:30:11', 1),
(741, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''6''', '2016-08-25', '11:30:13', 1),
(742, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''7''', '2016-08-25', '11:30:15', 1),
(743, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''8''', '2016-08-25', '11:30:19', 1),
(744, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''9''', '2016-08-25', '11:30:21', 1),
(745, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''10''', '2016-08-25', '11:30:23', 1),
(746, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''11''', '2016-08-25', '11:30:25', 1),
(747, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_equipes WHERE idequipe = ''12''', '2016-08-25', '11:30:27', 1),
(748, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:12', 1),
(749, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:41', 1),
(750, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:33:55', 1),
(751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:02', 1),
(752, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:09', 1),
(753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:15', 1),
(754, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:22', 1),
(755, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:47', 1),
(756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:34:56', 1),
(757, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:05', 1),
(758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:35:37', 1),
(759, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:08', 1),
(760, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:36:30', 1),
(761, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''1''', '2016-08-25', '11:36:53', 1),
(762, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''2''', '2016-08-25', '11:36:54', 1),
(763, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''3''', '2016-08-25', '11:36:56', 1),
(764, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''4''', '2016-08-25', '11:36:58', 1),
(765, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''5''', '2016-08-25', '11:37:01', 1),
(766, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''6''', '2016-08-25', '11:37:03', 1),
(767, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''7''', '2016-08-25', '11:37:05', 1),
(768, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''8''', '2016-08-25', '11:37:07', 1),
(769, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''9''', '2016-08-25', '11:37:09', 1),
(770, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''10''', '2016-08-25', '11:37:10', 1),
(771, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''11''', '2016-08-25', '11:37:12', 1),
(772, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''12''', '2016-08-25', '11:37:13', 1),
(773, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''13''', '2016-08-25', '11:37:15', 1),
(774, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''14''', '2016-08-25', '11:37:17', 1),
(775, 'EXCLUSÃO DO LOGIN 15, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''15''', '2016-08-25', '11:37:19', 1),
(776, 'EXCLUSÃO DO LOGIN 16, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''16''', '2016-08-25', '11:37:21', 1),
(777, 'EXCLUSÃO DO LOGIN 17, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''17''', '2016-08-25', '11:37:22', 1),
(778, 'EXCLUSÃO DO LOGIN 18, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''18''', '2016-08-25', '11:37:24', 1),
(779, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-25', '11:38:15', 1),
(780, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-25', '11:38:17', 1),
(781, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-08-25', '11:38:19', 1),
(782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:39:33', 1),
(783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:41:06', 1),
(784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:48:51', 1),
(785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:49:46', 1),
(786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:51:10', 1),
(787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:18', 1),
(788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '11:53:50', 1),
(789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:02:47', 1),
(790, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''39''', '2016-08-25', '12:02:52', 1),
(791, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''40''', '2016-08-25', '12:02:55', 1),
(792, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''41''', '2016-08-25', '12:02:57', 1),
(793, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''42''', '2016-08-25', '12:02:59', 1),
(794, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''43''', '2016-08-25', '12:03:01', 1),
(795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '12:03:41', 1),
(796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:14:52', 1),
(797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:15:10', 1),
(798, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''1''', '2016-08-25', '14:21:41', 1),
(799, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''2''', '2016-08-25', '14:21:45', 1),
(800, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''3''', '2016-08-25', '14:21:48', 1),
(801, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''4''', '2016-08-25', '14:21:50', 1),
(802, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''5''', '2016-08-25', '14:21:52', 1),
(803, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''6''', '2016-08-25', '14:21:54', 1),
(804, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''7''', '2016-08-25', '14:21:56', 1),
(805, 'CADASTRO DO CLIENTE ', '', '2016-08-25', '14:22:57', 1),
(806, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = ''8''', '2016-08-25', '14:23:51', 1),
(807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:32', 1),
(808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:43:55', 1),
(809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:17', 1),
(810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:44:30', 1),
(811, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''8''', '2016-08-25', '14:45:02', 1),
(812, 'EXCLUSÃO DO LOGIN 50, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''50''', '2016-08-25', '14:45:07', 1),
(813, 'EXCLUSÃO DO LOGIN 51, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''51''', '2016-08-25', '14:45:11', 1),
(814, 'EXCLUSÃO DO LOGIN 52, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''52''', '2016-08-25', '14:45:15', 1),
(815, 'EXCLUSÃO DO LOGIN 53, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''53''', '2016-08-25', '14:45:18', 1),
(816, 'EXCLUSÃO DO LOGIN 54, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''54''', '2016-08-25', '14:45:20', 1),
(817, 'EXCLUSÃO DO LOGIN 55, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = ''55''', '2016-08-25', '14:45:25', 1),
(818, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:01', 1),
(819, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:21', 1),
(820, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:49:47', 1),
(821, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:52:26', 1),
(822, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:55:43', 1),
(823, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:57:09', 1),
(824, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '14:59:15', 1),
(825, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:19', 1),
(826, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:00:59', 1),
(827, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:10:30', 1),
(828, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:12:32', 1),
(829, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:14', 1),
(830, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:19:41', 1),
(831, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:20:23', 1),
(832, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:21:01', 1),
(833, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-25', '15:41:28', 1),
(834, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:47', 1),
(835, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:32:58', 1),
(836, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:09', 1),
(837, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:17', 1),
(838, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:24', 1),
(839, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:32', 1),
(840, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:40', 1),
(841, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:49', 1),
(842, 'CADASTRO DO CLIENTE ', '', '2016-08-26', '15:33:56', 1),
(843, 'EXCLUSÃO DO LOGIN 151, NOME: , Email: ', 'DELETE FROM tb_atuacoes WHERE idatuacao = ''151''', '2016-08-26', '15:34:14', 1),
(844, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:36:16', 1),
(845, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:42:46', 1),
(846, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:05', 1),
(847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:22', 1),
(848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:33', 1),
(849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:43:46', 1),
(850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:16', 1),
(851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:44:52', 1),
(852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:45:04', 1),
(853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-26', '15:46:30', 1),
(854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '18:51:26', 0),
(855, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '18:51:30', 0),
(856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:16:59', 0),
(857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:18:46', 0),
(858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:21:34', 0),
(859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:24:24', 0),
(860, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '19:24:28', 0),
(861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:33:13', 0),
(862, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-08-27', '19:33:40', 0),
(863, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '19:41:11', 0),
(864, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-27', '19:41:14', 0),
(865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:42:48', 0),
(866, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:43:20', 0),
(867, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '19:43:24', 0),
(868, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-08-27', '19:43:27', 0),
(869, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:47:39', 0),
(870, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '19:47:43', 0),
(871, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:50:56', 0),
(872, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:54:50', 0),
(873, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-08-27', '19:54:56', 0),
(874, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-08-27', '19:54:58', 0),
(875, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '19:55:00', 0),
(876, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '19:56:22', 0),
(877, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:19:06', 0),
(878, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '20:19:10', 0),
(879, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:20:55', 0),
(880, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:22:38', 0),
(881, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-08-27', '20:22:41', 0),
(882, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:29:14', 0),
(883, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:32:24', 0),
(884, 'DESATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''2''', '2016-08-27', '20:32:43', 0),
(885, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:34:14', 0),
(886, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:47:12', 0),
(887, 'ATIVOU O LOGIN 2', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''2''', '2016-08-27', '20:47:22', 0),
(888, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:49:08', 0),
(889, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:53:49', 0),
(890, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '20:59:22', 0),
(891, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:01:55', 0),
(892, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-27', '21:05:47', 0),
(893, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:27:16', 0),
(894, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-28', '03:46:33', 0),
(895, 'ALTERAÇÃO DO CLIENTE ', '', '2016-08-29', '11:46:28', 1),
(896, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''77''', '2016-08-29', '11:46:37', 1),
(897, 'EXCLUSÃO DO LOGIN 78, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''78''', '2016-08-29', '11:46:39', 1),
(898, 'EXCLUSÃO DO LOGIN 79, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''79''', '2016-08-29', '11:46:41', 1),
(899, 'CADASTRO DO CLIENTE ', '', '2016-08-29', '11:49:11', 1),
(900, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '16:39:22', 13),
(901, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:47:03', 13),
(902, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:25', 13),
(903, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:41', 13),
(904, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-12', '19:48:52', 13),
(905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:26:42', 1),
(906, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:28:49', 1),
(907, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:35:08', 1),
(908, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '17:57:02', 1),
(909, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:27:02', 1),
(910, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:28:27', 1),
(911, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:31:51', 1),
(912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:35:11', 1),
(913, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:38:10', 1),
(914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '18:53:56', 1),
(915, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:01:02', 1),
(916, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:01:40', 1),
(917, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:03:58', 1),
(918, 'CADASTRO DO CLIENTE ', '', '2016-09-13', '19:04:19', 1),
(919, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:05:43', 1),
(920, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:09:40', 1),
(921, 'EXCLUSÃO DO LOGIN 74, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''74''', '2016-09-13', '19:09:48', 1),
(922, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:26', 1),
(923, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:11:47', 1),
(924, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:15:07', 1),
(925, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '19:57:44', 1),
(926, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-13', '20:01:10', 1),
(927, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:23:45', 1),
(928, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:24:20', 1),
(929, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:27:15', 1),
(930, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '09:33:52', 1),
(931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '10:39:26', 1),
(932, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:34:16', 1),
(933, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '11:46:08', 1),
(934, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '11:52:01', 1),
(935, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:07:45', 1),
(936, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:04', 1),
(937, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:42', 1),
(938, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '12:20:58', 1),
(939, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:11:57', 1),
(940, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '15:29:28', 1),
(941, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:29:48', 1),
(942, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:30:16', 1),
(943, 'CADASTRO DO CLIENTE ', '', '2016-09-15', '15:31:19', 1),
(944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '17:12:33', 1),
(945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-15', '19:50:15', 1),
(946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '08:37:22', 1),
(947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:01:49', 1),
(948, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:38:31', 1),
(949, 'EXCLUSÃO DO LOGIN 37, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''37''', '2016-09-16', '09:38:38', 1),
(950, 'EXCLUSÃO DO LOGIN 38, NOME: , Email: ', 'DELETE FROM tb_dicas WHERE iddica = ''38''', '2016-09-16', '09:38:41', 1),
(951, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '09:41:49', 1),
(952, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:26', 1),
(953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:06:41', 1),
(954, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:07:02', 1),
(955, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '11:54:08', 1),
(956, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:02:52', 1),
(957, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:03:05', 1),
(958, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '12:33:32', 1),
(959, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '15:53:42', 1),
(960, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-16', '19:20:41', 1),
(961, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '10:48:09', 1),
(962, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:49:25', 1),
(963, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '11:52:15', 1),
(964, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:08', 1),
(965, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '13:26:47', 1),
(966, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''1''', '2016-09-17', '14:48:21', 1),
(967, 'DESATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''2''', '2016-09-17', '14:48:29', 1),
(968, 'DESATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''NAO'' WHERE idunidade = ''1''', '2016-09-17', '14:48:34', 1),
(969, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '14:50:07', 1),
(970, 'ATIVOU O LOGIN 2', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''2''', '2016-09-17', '14:51:18', 1),
(971, 'ATIVOU O LOGIN 1', 'UPDATE tb_unidades SET ativo = ''SIM'' WHERE idunidade = ''1''', '2016-09-17', '14:51:21', 1),
(972, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:27:48', 1),
(973, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-17', '17:28:34', 1),
(974, 'CADASTRO DO CLIENTE ', '', '2016-09-17', '17:30:06', 1),
(975, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:13', 1),
(976, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:10:49', 1),
(977, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:11:03', 1),
(978, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:14:08', 1),
(979, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:11', 1),
(980, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:30', 1),
(981, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:54:48', 1),
(982, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '10:55:08', 1),
(983, 'CADASTRO DO CLIENTE ', '', '2016-09-18', '11:09:45', 1),
(984, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:43:17', 1),
(985, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:44:24', 1),
(986, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:51', 1),
(987, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:50:58', 1),
(988, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:07', 1),
(989, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '11:51:13', 1),
(990, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-18', '15:06:09', 1),
(991, 'EXCLUSÃO DO LOGIN 85, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''85''', '2016-09-19', '11:05:44', 1),
(992, 'EXCLUSÃO DO LOGIN 76, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''76''', '2016-09-19', '11:05:46', 1),
(993, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''83''', '2016-09-19', '11:05:49', 1),
(994, 'EXCLUSÃO DO LOGIN 84, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''84''', '2016-09-19', '11:05:52', 1),
(995, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:07:45', 1),
(996, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:13:50', 1),
(997, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:14:46', 1),
(998, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:15:35', 1),
(999, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:17:05', 1),
(1000, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:21:57', 1),
(1001, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:03', 1),
(1002, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:09', 1),
(1003, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:15', 1),
(1004, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '11:22:20', 1),
(1005, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:27:28', 1),
(1006, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '11:28:25', 1),
(1007, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:06:56', 1),
(1008, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:07:31', 1),
(1009, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:10:36', 1),
(1010, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:26', 1),
(1011, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '12:12:53', 1),
(1012, 'CADASTRO DO CLIENTE ', '', '2016-09-19', '13:55:28', 1),
(1013, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:36:48', 1),
(1014, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:08', 1),
(1015, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:40:15', 1),
(1016, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:01', 1),
(1017, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:42:07', 1),
(1018, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-19', '16:44:41', 1),
(1019, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:17', 1),
(1020, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '12:23:45', 1),
(1021, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:25', 1),
(1022, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:35', 1),
(1023, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:01:52', 1),
(1024, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '13:02:04', 1),
(1025, 'CADASTRO DO CLIENTE ', '', '2016-09-21', '14:58:35', 1),
(1026, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '15:45:26', 1),
(1027, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:06:57', 1),
(1028, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:07:12', 1),
(1029, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '18:51:52', 1),
(1030, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-21', '20:17:45', 1),
(1031, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '08:46:52', 1),
(1032, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '09:19:31', 1),
(1033, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '10:09:37', 1),
(1034, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '11:36:44', 1),
(1035, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:23', 1),
(1036, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:48:40', 1),
(1037, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:04', 1),
(1038, 'EXCLUSÃO DO LOGIN 39, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''39''', '2016-09-23', '12:49:16', 1),
(1039, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '12:49:42', 1),
(1040, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:07:13', 1),
(1041, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '13:20:03', 1),
(1042, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''40''', '2016-09-23', '14:07:52', 1),
(1043, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''41''', '2016-09-23', '14:07:56', 1),
(1044, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-23', '16:32:57', 1),
(1045, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:21:42', 1),
(1046, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:22:23', 1),
(1047, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-24', '12:23:20', 1),
(1048, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '19:59:32', 3),
(1049, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:05:57', 3),
(1050, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:06:32', 3),
(1051, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:04', 3),
(1052, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:20', 3),
(1053, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:07:39', 3),
(1054, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:11', 3),
(1055, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:38', 3),
(1056, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:08:55', 3),
(1057, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:15:17', 3),
(1058, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:15:33', 3),
(1059, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_unidades WHERE idunidade = ''3''', '2016-09-26', '20:15:38', 3),
(1060, 'ALTERAÇÃO DO CLIENTE ', '', '2016-09-26', '20:17:17', 3),
(1061, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:06:13', 3),
(1062, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:06:47', 3),
(1063, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:07:05', 3),
(1064, 'CADASTRO DO CLIENTE ', '', '2016-10-07', '22:07:34', 3),
(1065, 'CADASTRO DO CLIENTE ', '', '2016-10-07', '22:07:47', 3),
(1066, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:09:08', 3),
(1067, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-07', '22:09:27', 3),
(1068, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-08', '12:00:31', 3),
(1069, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-08', '12:03:43', 3),
(1070, 'DESATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''1''', '2016-10-09', '15:02:38', 3),
(1071, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:08:48', 3),
(1072, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:09:23', 3),
(1073, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:09:50', 3),
(1074, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:10:02', 3),
(1075, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:10:18', 3),
(1076, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:10:51', 3),
(1077, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:03', 3),
(1078, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:24', 3),
(1079, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:11:53', 3),
(1080, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:12:07', 3),
(1081, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:12:49', 3),
(1082, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:01', 3),
(1083, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:23', 3),
(1084, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:13:38', 3),
(1085, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:04', 3),
(1086, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:28', 3),
(1087, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:40', 3),
(1088, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:14:47', 3),
(1089, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:15:14', 3),
(1090, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:15:22', 3),
(1091, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:16:23', 3),
(1092, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:16:30', 3),
(1093, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:22:21', 3),
(1094, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:24:52', 3),
(1095, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:27:33', 3),
(1096, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:31:31', 3),
(1097, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:35:00', 3),
(1098, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:37:05', 3),
(1099, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:37:34', 3),
(1100, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:39:37', 3),
(1101, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:41:29', 3),
(1102, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:43:11', 3),
(1103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:44:09', 3),
(1104, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:47:04', 3),
(1105, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:49:03', 3),
(1106, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '12:50:09', 3),
(1107, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '12:50:43', 3),
(1108, 'EXCLUSÃO DO LOGIN 19, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''19''', '2016-10-11', '13:05:30', 3),
(1109, 'EXCLUSÃO DO LOGIN 20, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''20''', '2016-10-11', '13:05:32', 3),
(1110, 'EXCLUSÃO DO LOGIN 21, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''21''', '2016-10-11', '13:05:34', 3),
(1111, 'EXCLUSÃO DO LOGIN 22, NOME: , Email: ', 'DELETE FROM tb_clientes WHERE idcliente = ''22''', '2016-10-11', '13:05:36', 3),
(1112, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:44:02', 3),
(1113, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:45:48', 3),
(1114, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:48:25', 3),
(1115, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '14:48:50', 3),
(1116, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:51:01', 3),
(1117, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:53:55', 3),
(1118, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:56:29', 3),
(1119, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:58:04', 3),
(1120, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '14:59:16', 3),
(1121, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '14:59:45', 3),
(1122, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:01:09', 3),
(1123, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:01:33', 3),
(1124, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:02:41', 3),
(1125, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:03:01', 3),
(1126, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:04:15', 3),
(1127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:04:33', 3),
(1128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:04:55', 3),
(1129, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:07:10', 3),
(1130, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:09:03', 3),
(1131, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:10:47', 3),
(1132, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:12:13', 3),
(1133, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:13:38', 3),
(1134, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:14:55', 3),
(1135, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:15:15', 3),
(1136, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:16:36', 3),
(1137, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '15:17:59', 3),
(1138, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:22:48', 3),
(1139, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:25:17', 3),
(1140, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:26:00', 3),
(1141, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:28:06', 3),
(1142, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:28:55', 3),
(1143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:46:06', 3),
(1144, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:47:39', 3),
(1145, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:48:02', 3),
(1146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:49:01', 3),
(1147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '15:49:37', 3),
(1148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '18:32:33', 3),
(1149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:20:42', 3),
(1150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:27:20', 3),
(1151, 'CADASTRO DO CLIENTE ', '', '2016-10-11', '19:29:11', 3),
(1152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '19:30:38', 3),
(1153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:25:13', 3),
(1154, 'ATIVOU O LOGIN 1', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''1''', '2016-10-11', '22:25:17', 3),
(1155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:30:29', 3),
(1156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:31:54', 3),
(1157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:40:25', 3),
(1158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:50:00', 3),
(1159, 'DESATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''3''', '2016-10-11', '22:50:08', 3),
(1160, 'DESATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''4''', '2016-10-11', '22:50:10', 3),
(1161, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:51:37', 3),
(1162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '22:54:18', 3),
(1163, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '23:04:12', 3),
(1164, 'ATIVOU O LOGIN 3', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''3''', '2016-10-11', '23:04:16', 3),
(1165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-11', '23:09:40', 3),
(1166, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:22:21', 3),
(1167, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:23:46', 3),
(1168, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:24:49', 3),
(1169, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:25:46', 3),
(1170, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:26:22', 3),
(1171, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:27:03', 3),
(1172, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:27:55', 3),
(1173, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:28:45', 3),
(1174, 'CADASTRO DO CLIENTE ', '', '2016-10-14', '11:29:31', 3),
(1175, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:45:46', 1),
(1176, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:47:20', 1),
(1177, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:47:46', 1),
(1178, 'DESATIVOU O LOGIN 31', 'UPDATE tb_clientes SET ativo = ''NAO'' WHERE idcliente = ''31''', '2016-10-25', '16:48:23', 1),
(1179, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:49:14', 1),
(1180, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:49:39', 1),
(1181, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:12', 1),
(1182, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:36', 1),
(1183, 'CADASTRO DO CLIENTE ', '', '2016-10-25', '16:50:59', 1),
(1184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '16:55:28', 1),
(1185, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '16:58:06', 1),
(1186, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:11:41', 1),
(1187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:33:20', 1),
(1188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:47:05', 1),
(1189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:47:14', 1),
(1190, 'DESATIVOU O LOGIN 11', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''11''', '2016-10-25', '17:47:19', 1),
(1191, 'DESATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = ''NAO'' WHERE idbanner = ''12''', '2016-10-25', '17:47:21', 1),
(1192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:56:16', 1),
(1193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '17:57:49', 1),
(1194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:01:26', 1),
(1195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:14:28', 1),
(1196, 'ATIVOU O LOGIN 11', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''11''', '2016-10-25', '18:14:32', 1),
(1197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-25', '18:26:53', 1),
(1198, 'ATIVOU O LOGIN 12', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''12''', '2016-10-25', '18:26:57', 1),
(1199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:03:00', 1),
(1200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:33:00', 1),
(1201, 'ATIVOU O LOGIN 4', 'UPDATE tb_banners SET ativo = ''SIM'' WHERE idbanner = ''4''', '2016-10-26', '18:33:05', 1),
(1202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:33:55', 1),
(1203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-26', '18:40:01', 1),
(1204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:10:45', 1),
(1205, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:03', 1),
(1206, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:10', 1),
(1207, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '14:11:18', 1),
(1208, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:29:57', 1),
(1209, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:30:44', 1),
(1210, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:32:35', 1),
(1211, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:34:08', 1),
(1212, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:35:40', 1),
(1213, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:36:34', 1),
(1214, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:47:29', 1),
(1215, 'ALTERAÇÃO DO CLIENTE ', '', '2016-10-27', '15:49:00', 1),
(1216, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_galeria_empresa WHERE idgaleriaempresa = ''1''', '2016-10-27', '15:49:32', 1),
(1217, 'CADASTRO DO CLIENTE ', '', '2016-10-27', '15:49:57', 1),
(1218, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_galerias WHERE idgaleria = ''45''', '2016-10-27', '15:50:31', 1),
(1219, 'CADASTRO DO CLIENTE ', '', '2016-10-27', '15:50:45', 1),
(1220, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '22:58:20', 1),
(1221, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '22:58:37', 1),
(1222, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:08:08', 1),
(1223, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '23:08:51', 1),
(1224, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:17:24', 1),
(1225, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:25:59', 1),
(1226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-24', '23:28:19', 1),
(1227, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:32:55', 1),
(1228, 'CADASTRO DO CLIENTE ', '', '2016-11-24', '23:38:41', 1),
(1229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-26', '03:07:31', 1),
(1230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '10:54:44', 1),
(1231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '10:55:21', 1),
(1232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:02:08', 1),
(1233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:02:21', 1),
(1234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:05:57', 1),
(1235, 'ALTERAÇÃO DO CLIENTE ', '', '2016-11-28', '11:06:35', 1),
(1236, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:26:55', 3),
(1237, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:27:52', 3),
(1238, 'CADASTRO DO CLIENTE ', '', '2016-12-03', '15:28:19', 3),
(1239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:10:04', 3),
(1240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:11:06', 3),
(1241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:12:36', 3),
(1242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:12:56', 3),
(1243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:17', 3),
(1244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:28', 3),
(1245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:40', 3),
(1246, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:13:55', 3),
(1247, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:30:08', 3),
(1248, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:30:51', 3),
(1249, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:18', 3),
(1250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:35', 3),
(1251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:31:49', 3),
(1252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:06', 3),
(1253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:29', 3),
(1254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:32:50', 3),
(1255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:33:29', 3),
(1256, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:34:11', 3),
(1257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:52:09', 3),
(1258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:53:50', 3),
(1259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '12:54:26', 3),
(1260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:17:32', 3),
(1261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:17:49', 3),
(1262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-04', '13:18:59', 3),
(1263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:55:39', 3),
(1264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:55:58', 3),
(1265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:56:08', 3),
(1266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '09:56:17', 3),
(1267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '11:15:39', 3),
(1268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '11:33:15', 3),
(1269, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '12:09:39', 3),
(1270, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '13:14:03', 3),
(1271, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '14:12:12', 3),
(1272, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '15:56:06', 3),
(1273, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '16:42:24', 3),
(1274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '17:53:31', 3),
(1275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:02:16', 3),
(1276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:31:59', 3),
(1277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '18:52:22', 3),
(1278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-05', '19:41:09', 3),
(1279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '00:13:19', 3),
(1280, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:39:49', 3),
(1281, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:39:59', 3),
(1282, 'CADASTRO DO CLIENTE ', '', '2016-12-06', '12:40:06', 3),
(1283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:41:05', 3),
(1284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:46:57', 3),
(1285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:47:25', 3),
(1286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-06', '23:47:42', 3),
(1287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:42:08', 3),
(1288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:43:18', 3),
(1289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '10:44:05', 3),
(1290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:03:32', 3),
(1291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-12-07', '11:37:32', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_lojas`
--

CREATE TABLE IF NOT EXISTS `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_noticias`
--

CREATE TABLE IF NOT EXISTS `tb_noticias` (
  `idnoticia` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'SELO QUALISOL Programa de Qualificação de Fornecedores de Sistemas de Aquecimento Solar', '<p style="text-align: justify;">\r\n	O QUALISOL BRASIL &eacute; o Programa de Qualifica&ccedil;&atilde;o de Fornecedores de Sistemas de Aquecimento Solar, que engloba fabricantes, revendas e instaladoras. Fruto de um conv&ecirc;nio entre a ABRAVA, o INMETRO e o PROCEL/Eletrobras, o programa tem como objetivo garantir ao consumidor qualidade dos fornecedores de sistemas de aquecimento solar, de modo a permitir:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o do conhecimento de fornecedores em rela&ccedil;&atilde;o ao aquecimento solar;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o da base de mercado do aquecimento solar e suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	O aumento da qualidade das instala&ccedil;&otilde;es e conseq&uuml;ente satisfa&ccedil;&atilde;o do consumidor final;</p>\r\n<p style="text-align: justify;">\r\n	Uma melhor e mais duradoura reputa&ccedil;&atilde;o e confian&ccedil;a em sistemas de aquecimento solar nas suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	Um crescente interesse e habilidade dos fornecedores na prospec&ccedil;&atilde;o de novos clientes e est&iacute;mulo ao surgimento de novos empreendedores.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Assim como em diversos pa&iacute;ses do mundo, no Brasil, as revendas e instaladores representam uma posi&ccedil;&atilde;o estrat&eacute;gica com rela&ccedil;&atilde;o &agrave; difus&atilde;o do aquecimento solar. Na maioria das vezes est&atilde;o em contato direto com o consumidor no momento de decis&atilde;o de compra e instala&ccedil;&atilde;o e algumas vezes tamb&eacute;m planejam e entregam os equipamentos e s&atilde;o respons&aacute;veis diretos por garantir uma instala&ccedil;&atilde;o qualificada com funcionamento, durabilidade e est&eacute;tica assegurados e comprovados. Al&eacute;m das revendas e instaladoras, as empresas fabricantes de equipamentos solares tamb&eacute;m realizam instala&ccedil;&otilde;es e contratos diretos com consumidores finais e assumem a responsabilidade por todo o processo desde a venda at&eacute; a instala&ccedil;&atilde;o e p&oacute;s-venda.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Portanto, a qualidade dos sistemas de aquecimento solar depende diretamente de:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	bons produtos, etiquetados;</p>\r\n<p style="text-align: justify;">\r\n	bons projetistas e revendas, qualificadas;</p>\r\n<p style="text-align: justify;">\r\n	bons instaladores, qualificados;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '0206201605386174124988..jpg', 'SIM', NULL, 'selo-qualisol-programa-de-qualificacao-de-fornecedores-de-sistemas-de-aquecimento-solar', '', '', '', NULL),
(50, 'ENERGIA RENOVÁVEL NO BRASIL - NÃO BASTA SOMENTE GERAR, TEMOS QUE SABER USÁ-LA', '<p>\r\n	A gera&ccedil;&atilde;o de energia renov&aacute;vel mundial apresentar&aacute; forte crescimento nos pr&oacute;ximos anos, com expectativa de crescimento de 12,7% no per&iacute;odo entre 2010 a 2013 segundo a International Energy Agency - IEA. As raz&otilde;es principais dessa previs&atilde;o s&atilde;o as metas de redu&ccedil;&atilde;o de emiss&otilde;es de CO2 e mudan&ccedil;as clim&aacute;ticas; melhorias tecnol&oacute;gicas favorecendo novas alternativas; aumento na demanda de energia; ambiente regulat&oacute;rio mais favor&aacute;vel; e incentivos governamentais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Enquanto os benef&iacute;cios de desenvolver-se uma agenda s&oacute;lida para consolida&ccedil;&atilde;o da energia renov&aacute;vel no Brasil s&atilde;o evidentes, &eacute; preciso atentar-se para os riscos associados &agrave; segrega&ccedil;&atilde;o do tema de energias renov&aacute;veis do panorama geral da agenda energ&eacute;tica no Brasil, atualmente levada pela ANP (Ag&ecirc;ncia Nacional do Petr&oacute;leo, G&aacute;s Natural e Biocombust&iacute;veis) e ANEEL (Ag&ecirc;ncia Nacional de Energia El&eacute;trica. &Eacute; preciso existir um incentivo maior &agrave;s pol&iacute;ticas p&uacute;blicas que tornem economicamente vi&aacute;veis melhorias de projetos n&atilde;o s&oacute; voltadas para reduzir os gastos com energia el&eacute;trica, aumentando a efici&ecirc;ncia energ&eacute;tica dos processos, como tamb&eacute;m que possibilitem o uso sustent&aacute;vel dos recursos naturais, diz Ricardo Antonio do Esp&iacute;rito Santo Gomes, consultor em efici&ecirc;ncia energ&eacute;tica do CTE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para Gomes &eacute; preciso pensar mais na maneira como usamos a energia, n&atilde;o somente na maneira como a geramos: N&atilde;o adianta gerarmos muita energia de forma eficiente se aqui na ponta usamos chuveiro el&eacute;trico, ao inv&eacute;s de aquecedor solar t&eacute;rmico por exemplo. Portanto, a gera&ccedil;&atilde;o distribu&iacute;da &eacute; a maneira mais eficiente de se garantir um crescimento sustent&aacute;vel para a sociedade, diminuindo perdas em transmiss&atilde;o, diminuindo o impacto ambiental de grandes centrais geradoras de energia e produzindo, localmente, as utilidades de que o cliente final precisa, como energia el&eacute;trica, vapor, &aacute;gua quente e &aacute;gua gelada, garantindo que um combust&iacute;vel se transforme ao m&aacute;ximo poss&iacute;vel, em outras formas de energia, causando menor impacto poss&iacute;vel e garantindo o n&iacute;vel de conforto exigido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele acrescenta que na pauta de pesquisas e desenvolvimento est&atilde;o os sistemas que possuem o atrativo de manterem altas taxas de efici&ecirc;ncia energ&eacute;tica, baixa emiss&atilde;o de poluentes e de CO2 e redu&ccedil;&atilde;o de custos de transmiss&atilde;o.</p>\r\n<div>\r\n	&nbsp;</div>', '0206201605489606782930..jpg', 'SIM', NULL, 'energia-renovavel-no-brasil--nao-basta-somente-gerar-temos-que-saber-usala', '', '', '', NULL),
(48, 'BANHO AQUECIDO ATRAVÉS DE AQUECIMENTO SOLAR', '<p>\r\n	Apesar de abundante, a energia solar como energia t&eacute;rmica &eacute; pouco aproveitada no Brasil, com isso o banho quente brasileiro continua, em 67% dos lares brasileiros, a utilizar o chuveiro el&eacute;trico que consome 8% da eletricidade produzida ao inv&eacute;s do aquecedor solar, segundo dados do Instituto Vitae Civillis &quot;Um Banho de Sol para o Brasil&quot;, de D&eacute;lcio Rodrigues e Roberto Matajs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esses n&uacute;meros contrastam com a capacidade que o territ&oacute;rio brasileiro tem de produzir energia solar: O potencial de gera&ccedil;&atilde;o &eacute; equivalente a 15 trilh&otilde;es de MW/h, correspondente a 50 mil vezes o consumo nacional de eletricidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O projeto Cidade Solares, parceria entre o Vitae Civilis e a DASOL/ABRAVA*, foi criado para discutir, propor e acompanhar a tramita&ccedil;&atilde;o e entrada em vigor de leis que incentivam ou obrigam o uso de sistemas solares de aquecimento de &aacute;gua nas cidades e estados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A id&eacute;ia &eacute; debater e difundir conhecimento sobre o assunto, al&eacute;m de analisar as possibilidades de implanta&ccedil;&atilde;o de projetos nas cidades. Ao ganhar o t&iacute;tulo de &quot;Solar&quot;, uma cidade servir&aacute; de exemplo para outras, com a difus&atilde;o do tema e das tecnologias na pr&aacute;tica. No site, o projeto lista v&aacute;rias iniciativas de cidades que implantaram com sucesso sistemas solares, como Freiburg- na Alemanha, Graz- na &Aacute;ustria, Portland- nos EUA e Oxford- na Inglaterra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* DASOL/ ABRAVA: Departamento Nacional de Aquecimento Solar da Associa&ccedil;&atilde;o Brasileira de Refrigera&ccedil;&atilde;o, Ar-condicionado, Ventila&ccedil;&atilde;o e Aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte: Revista Super Interessante - Editora Abril</p>', '0206201605411524312164..jpg', 'SIM', NULL, 'banho-aquecido-atraves-de-aquecimento-solar', '', '', '', NULL),
(47, 'COMO FUNCIONA UM SISTEMA DE AQUECIMENTO SOLAR DE ÁGUA? VOCÊ SABE?', '<p>\r\n	A mesma energia solar que ilumina e aquece o planeta pode ser usada para esquentar a &aacute;gua dos nossos banhos, acenderem l&acirc;mpadas ou energizar as tomadas de casa. O sol &eacute; uma fonte inesgot&aacute;vel de energia e, quando falamos em sustentabilidade, em economia de recursos e de &aacute;gua, em economia de energia e redu&ccedil;&atilde;o da emiss&atilde;o de g&aacute;s carb&ocirc;nico na atmosfera, nada mais natural do que pensarmos numa maneira mais eficiente de utiliza&ccedil;&atilde;o da energia solar. Esta energia &eacute; totalmente limpa e, principalmente no Brasil, onde temos uma enorme incid&ecirc;ncia solar, os sistemas para o aproveitamento da energia do sol s&atilde;o muito eficientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para ser utilizada, a energia solar deve ser transformada e, para isto, h&aacute; duas maneiras principais de realizar essa transforma&ccedil;&atilde;o: Os pain&eacute;is fotovoltaicos e os aquecedores solares.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os primeiros s&atilde;o respons&aacute;veis pela transforma&ccedil;&atilde;o da energia solar em energia el&eacute;trica. Com esses pain&eacute;is podemos utilizar o sol para acender as l&acirc;mpadas das nossas casas ou para ligar uma televis&atilde;o. A segunda forma &eacute; com o uso de aquecedores solares, que utiliza a energia solar para aquecer a &aacute;gua que ser&aacute; direcionada aos chuveiros ou piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como funciona um sistema de aquecimento solar?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Basicamente este sistema &eacute; composto por dois elementos: Os coletores solares (pain&eacute;is de capta&ccedil;&atilde;o, que vemos freq&uuml;entemente nos telhados das casas) e o reservat&oacute;rio de &aacute;gua quente, tamb&eacute;m chamado de boiler.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os coletores s&atilde;o formados por uma placa de vidro que isola do ambiente externo aletas de cobre ou alum&iacute;nio pintadas com tintas especiais na cor escura para que absorvam o m&aacute;ximo da radia&ccedil;&atilde;o. Ao absorver a radia&ccedil;&atilde;o, estas aletas deixam o calor passar para tubos em forma de serpentina geralmente feitos de cobre. Dentro desses tubos passa &aacute;gua, que &eacute; aquecida antes de ser levada para o reservat&oacute;rio de &aacute;gua quente. Estas placas coletoras podem ser dispostas sobre telhados e lajes e a quantidade de placas instaladas varia conforme o tamanho do reservat&oacute;rio, o n&iacute;vel de insola&ccedil;&atilde;o da regi&atilde;o e as condi&ccedil;&otilde;es de instala&ccedil;&atilde;o. No hemisf&eacute;rio sul, normalmente as placas ficam inclinadas para a dire&ccedil;&atilde;o norte para receber a maior quantidade poss&iacute;vel de radia&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios s&atilde;o cilindros de alum&iacute;nio, inox e polipropileno com isolantes t&eacute;rmicos que mant&eacute;m pelo maior tempo poss&iacute;vel a &aacute;gua aquecida. Uma caixa de &aacute;gua fria abastece o sistema para que o boiler fique sempre cheio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios devem ser instalados o mais pr&oacute;ximo poss&iacute;vel das placas coletoras (para evitar perda de efici&ecirc;ncia do sistema), de prefer&ecirc;ncia devem estar sob o telhado (para evitar a perda de calor para a atmosfera) e em n&iacute;vel um pouco elevado. Dessa forma, consegue-se o efeito chamado de termossif&atilde;o, ou seja, conforme a &aacute;gua dos coletores vai esquentando, ela torna-se menos densa e vai sendo empurrada pela &aacute;gua fria. Assim ela sobe e chega naturalmente ao boiler, sem a necessidade de bombeamento. Em casos espec&iacute;ficos, em que o reservat&oacute;rio n&atilde;o possa ser instalado acima das placas coletoras, podem-se utilizar bombas para promover a circula&ccedil;&atilde;o da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E nos dias nublados, chuvosos ou &agrave; noite?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apesar de poderem ser instalados de forma independente, os aquecedores solares normalmente trabalham com um sistema auxiliar de aquecimento da &aacute;gua. Este sistema pode ser el&eacute;trico ou a g&aacute;s, e j&aacute; vem de f&aacute;brica. Quando houver uma seq&uuml;&ecirc;ncia de dias nublados ou chuvosos em que a energia gerada pelo aquecedor solar n&atilde;o seja suficiente para esquentar toda a &aacute;gua necess&aacute;ria para o consumo di&aacute;rio, um aquecedor el&eacute;trico ou a g&aacute;s &eacute; acionado, gerando &aacute;gua quente para as pias e chuveiros. O mesmo fato acontece &agrave; noite quando n&atilde;o houver mais &aacute;gua aquecida no reservat&oacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Num pa&iacute;s tropical como o nosso, na grande maioria dos dias a &aacute;gua ser&aacute; aquecida com a energia solar e, portanto, os sistemas auxiliares ficar&atilde;o desligados a maior parte do tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena economicamente?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O custo do sistema &eacute; compensado pela economia mensal na conta de energia el&eacute;trica. O Aquecedor Solar Heliotek reduz at&eacute; 80% dos custos de energia com aquecimento e em poucos anos o sistema se paga, gerando lucro ao longo de sua vida &uacute;til? 20 anos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os aquecedores solares podem ser instalados em novas obras ou durante reformas. Ao construir ou reformar, estude esta op&ccedil;&atilde;o, que &eacute; boa para o planeta e pode ser &oacute;tima para a sua conta de energia.</p>', '0206201605446692289010..jpg', 'SIM', NULL, 'como-funciona-um-sistema-de-aquecimento-solar-de-agua-voce-sabe', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_parceiros`
--

CREATE TABLE IF NOT EXISTS `tb_parceiros` (
  `idparceiro` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_parceiros`
--

INSERT INTO `tb_parceiros` (`idparceiro`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `url`) VALUES
(1, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', '', '', '', 'http://www.uol.com.br'),
(2, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(3, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(4, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(5, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(6, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(7, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(8, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(9, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(10, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(11, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(12, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(13, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(14, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(15, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(16, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, ''),
(17, 'Parceiro 1', NULL, '3007201607421377805203..jpg', 'SIM', 0, 'parceiro-1', NULL, NULL, NULL, 'http://www.uol.com.br'),
(18, 'Parceiro 2', NULL, '3007201607421377805203..jpg', 'SIM', 0, NULL, NULL, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE IF NOT EXISTS `tb_portfolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `codigo_produto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exibir_home` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `modelo_produto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `id_categoriaproduto`, `id_subcategoriaproduto`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `modelo`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `codigo_produto`, `exibir_home`, `modelo_produto`) VALUES
(9, 'DECK PISCINA', '0412201612301172463605..jpg', '<p>\r\n	Gola Polo Tradicional Branca Manga Curta</p>\r\n<p>\r\n	Tecido: Piquet de PV 67% Poliester , 33% Viscose</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Cores Dispon&iacute;veis:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Preto</p>\r\n<p>\r\n	Branco</p>\r\n<p>\r\n	Amarelo Can&aacute;rio</p>\r\n<p>\r\n	Azul Celeste</p>\r\n<p>\r\n	Azul Royal</p>\r\n<p>\r\n	Azul Petr&oacute;leo</p>\r\n<p>\r\n	Amarelo Ouro</p>\r\n<div>\r\n	&nbsp;</div>', 86, 1, '', '', '', 'SIM', 0, 'deck-piscina', '4545454545454545', NULL, NULL, NULL, NULL, '050150031', 'SIM', 'MODELO 0020'),
(10, 'PRODUTO 1 áéíóú', '0412201612301133534618..jpg', '<p>\r\n	Avental Manga Longa Gabardine Gola Padre</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecido: Gabardine 100 % Poli&eacute;ster</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cores Dispon&iacute;veis: Branco</p>', 75, 6, '', '', '', 'SIM', 0, 'produto-1-aeiou', NULL, NULL, NULL, NULL, NULL, 'P65MBR', 'SIM', 'MODELO 928'),
(11, 'PRODUTO 2 áéíóú', '0412201612311255599267..jpg', '<p>\r\n	Avental Envelope</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecido: Brim Leve 100% Algod&atilde;o</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cores Dispon&iacute;veis:&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Azul Royal</p>\r\n<p>\r\n	Branco</p>\r\n<p>\r\n	Rosa</p>\r\n<p>\r\n	Preto</p>\r\n<p>\r\n	Marron</p>\r\n<p>\r\n	Azul Celeste</p>\r\n<p>\r\n	Laranja</p>\r\n<p>\r\n	Amarelo Ouro</p>\r\n<p>\r\n	Amarelo Can&aacute;rio</p>', 88, 3, '', '', '', 'SIM', 0, 'produto-2-aeiou', NULL, NULL, NULL, NULL, NULL, '040010011', 'SIM', 'MODELO 846'),
(12, 'PRODUTO 3', '0412201612311361664280..jpg', '<div>\r\n	Avental kit X com Bandana Estampado</div>', 88, 2, '', '', '', 'SIM', 0, 'produto-3', NULL, NULL, NULL, NULL, NULL, '040010005', 'SIM', 'MODELO 434'),
(13, 'PRODUTO 4', '0412201612321222247140..jpg', '<p>\r\n	Dolma Feminina</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecido: Brim ou Fust&atilde;o 97% Algod&atilde;o, 3% Elastano (Fust&atilde;o)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cores: Branco e Preto</p>', 89, 3, '', '', '', 'SIM', 0, 'produto-4', NULL, NULL, NULL, NULL, NULL, '040340025', 'SIM', 'MODELO 388'),
(14, 'PRODUTO 5', '0412201612321134120553..jpg', '<p>\r\n	Dolma Masculina</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecido: Fust&atilde;o 100% Algod&atilde;o</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cores: Branco e Preto</p>', 87, 3, '', '', '', 'SIM', 0, 'produto-5', NULL, NULL, NULL, NULL, NULL, '040340008', 'SIM', 'MODELO 8973'),
(15, 'PRODUTO 6', '0412201612321339317012..jpg', '<p>\r\n	Dolma Masculino</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Tecido: Fust&atilde;o 100% Algod&atilde;o</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cores: Branco Gelo</p>\r\n<div>\r\n	&nbsp;</div>', 88, 3, '', '', '', 'SIM', 0, 'produto-6', NULL, NULL, NULL, NULL, NULL, '040340029', 'SIM', 'MODELO 5761'),
(16, 'PRODUTO 7', '0412201612331132670787..jpg', '<p>\r\n	Kit Avental Envelope com Bandana Estampado</p>\r\n<p>\r\n	Tecido: Tricoline 100 % Algod&atilde;o</p>', 87, 3, '', '', '', 'SIM', 0, 'produto-7', NULL, NULL, NULL, NULL, NULL, '040010002', 'SIM', 'MODELO 83764');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_aplicacoes`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_aplicacoes` (
  `idprodutoaplicacao` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `titulo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_produtos_aplicacoes`
--

INSERT INTO `tb_produtos_aplicacoes` (`idprodutoaplicacao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `titulo`) VALUES
(11, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 1'),
(12, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 2'),
(13, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 3'),
(14, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 4'),
(15, NULL, 'SIM', NULL, NULL, 1, 'Aplicação 5');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos_modelos_aceitos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos_modelos_aceitos` (
  `idprodutosmodeloaceito` int(11) NOT NULL,
  `id_produto` int(11) NOT NULL,
  `id_tipoveiculo` int(11) NOT NULL,
  `id_marcaveiculo` int(11) NOT NULL,
  `id_modeloveiculo` int(11) NOT NULL,
  `ano_inicial` int(11) NOT NULL,
  `ano_final` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(11) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_produtos_modelos_aceitos`
--

INSERT INTO `tb_produtos_modelos_aceitos` (`idprodutosmodeloaceito`, `id_produto`, `id_tipoveiculo`, `id_marcaveiculo`, `id_modeloveiculo`, `ano_inicial`, `ano_final`, `ativo`, `ordem`, `url_amigavel`) VALUES
(3, 16, 1, 6, 4, 1987, 2013, 'SIM', 0, ''),
(4, 16, 1, 7, 5, 1987, 2013, 'SIM', 0, ''),
(5, 16, 1, 8, 6, 1987, 2013, 'SIM', 0, ''),
(6, 16, 1, 9, 7, 1987, 2013, 'SIM', 0, ''),
(7, 15, 1, 7, 5, 2000, 2016, 'SIM', 0, ''),
(8, 15, 1, 8, 6, 2000, 2016, 'SIM', 0, ''),
(10, 2, 1, 6, 3, 1990, 2016, 'SIM', 0, ''),
(11, 4, 3, 5, 2, 1994, 2004, 'SIM', 0, ''),
(12, 3, 1, 7, 5, 1980, 2016, 'SIM', 0, ''),
(13, 17, 8, 7, 5, 2009, 2013, 'SIM', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE IF NOT EXISTS `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index', NULL, NULL, 'SIM', NULL, NULL),
(2, 'Empresa', 'Empresa', NULL, NULL, 'SIM', NULL, NULL),
(3, 'Dicas', 'Dicas', NULL, NULL, 'SIM', NULL, NULL),
(5, 'Orçamentos', 'Orçamentos', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Serviços', 'Serviços', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Produtos', 'Produtos', NULL, NULL, 'SIM', NULL, NULL),
(8, 'Fale Conosco', 'Fale Conosco', NULL, NULL, 'SIM', NULL, NULL),
(9, 'Trabalhe Conosco', 'Trabalhe Conosco', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem_principal` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`, `imagem_principal`) VALUES
(5, 'Prensagem em mangueiras', '<p>\r\n	Prensagem em mangueiras e mangotes: Realizamos a prensagem das mangueiras hidr&aacute;ulicas atrav&eacute;s de maquinas que exercem press&atilde;o e inserem os terminais nas mangueiras, sendo calculada a press&atilde;o utilizada na coloca&ccedil;&atilde;o dos terminais hidr&aacute;ulicos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rainha da Borracha Goi&acirc;nia atende setores industriais, agr&iacute;cola, sucroalcooleiro e automotivo, fornecendo mangueiras montadas, conforme a necessidade de cada cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Estamos prontos para atend&ecirc;-lo!</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', '', 'SIM', NULL, 'prensagem-em-mangueiras', '', '', '', 0, '', '', '0206201611176686009934.png', '2508201602594049189843.jpg'),
(6, 'Vulcanização em correias', '<p>\r\n	Vulcaniza&ccedil;&atilde;o em correias de borracha, PVC e PU.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Rainha da Borracha atua no ramo de vulcaniza&ccedil;&atilde;o em correia transportadora, emenda a frio e emenda a quente, e possui a melhor equipe de profissionais do mercado com experi&ecirc;ncia e profissionalismo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Estamos prontos para atend&ecirc;-lo!</p>', '', 'SIM', NULL, 'vulcanizacao-em-correias', '', '', '', 0, '', '', '0206201605327691842909.png', '2508201603107466292261.jpg'),
(7, 'Emborrachamento de rolos', '<p>\r\n	Emborrachamento de rolos e roletes a frio e a quente para a aplica&ccedil;&atilde;o nas ind&uacute;strias de embalagens, couro, gr&aacute;fica, litogr&aacute;fica, metalgr&aacute;fica, siderurgica, pl&aacute;stico, t&ecirc;xtil, vidro, m&oacute;veis, papel e celulose e outros.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Todas as caracter&iacute;sticas t&eacute;cnicas das borrachas utilizadas, seguem rigorosamente as especifica&ccedil;&otilde;es de normas ASTM D-2000, ABNT EB 362 e SAE J 200 ou requisitos do cliente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Estamos prontos para atend&ecirc;-lo!</p>', '', 'SIM', NULL, 'emborrachamento-de-rolos', '', '', '', 0, '', '', '0206201611176686009934.png', '2508201603125891686056.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'ENFERMEIRO', 75, 'SIM', NULL, 'enfermeiro'),
(2, 'GARÇONETE', 88, 'SIM', NULL, 'garconete'),
(3, 'CHEF DE COZINHA', 88, 'SIM', NULL, 'chef-de-cozinha'),
(4, 'GARÇON', 88, 'SIM', NULL, 'garcon'),
(5, 'SECRETÁRIA DO LAR', 87, 'SIM', NULL, 'secretaria-do-lar'),
(6, 'MEDICO', 75, 'SIM', NULL, 'medico'),
(7, 'COPEIRA', 86, 'SIM', NULL, 'copeira'),
(8, 'PEDREIRO', 89, 'SIM', NULL, 'pedreiro'),
(9, 'MESTRE DE OBRA', 89, 'SIM', NULL, 'mestre-de-obra'),
(10, 'SERVENTE DE PEDREIRO', 89, 'SIM', NULL, 'servente-de-pedreiro'),
(11, 'FEMININO', 90, 'SIM', NULL, 'feminino'),
(12, 'MASCULINO', 90, 'SIM', NULL, 'masculino'),
(13, 'ACESSÓRIOS', 91, 'SIM', NULL, 'acessorios'),
(14, 'CALÇADOS', 91, 'SIM', NULL, 'calcados');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_tipos_veiculos`
--

CREATE TABLE IF NOT EXISTS `tb_tipos_veiculos` (
  `idtipoveiculo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `ordem` int(11) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `url_amigavel` varchar(255) NOT NULL,
  `imagem` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_tipos_veiculos`
--

INSERT INTO `tb_tipos_veiculos` (`idtipoveiculo`, `titulo`, `ordem`, `ativo`, `url_amigavel`, `imagem`) VALUES
(1, 'Carros', 0, 'SIM', 'carros', '2006201608061191940846.png'),
(2, 'Motos', 0, 'SIM', 'motos', '2006201608071116286885.png'),
(3, 'Caminhões', 0, 'SIM', 'caminhoes', '2006201608071262816952.png'),
(8, 'Naúticos', 0, 'SIM', 'nauticos', '2006201608081392104809.png'),
(9, 'Nobreaks', 0, 'SIM', 'nobreaks', '2006201608091390486381.png'),
(10, 'Lâmpadas', 0, 'SIM', 'lampadas', '2006201608101362518866.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_unidades`
--

CREATE TABLE IF NOT EXISTS `tb_unidades` (
  `idunidade` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `src_place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_unidades`
--

INSERT INTO `tb_unidades` (`idunidade`, `titulo`, `src_place`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'GOIÂNIA', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d30576.35403215722!2d-49.28782860624648!3d-16.67466878073772!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ef14fb6786dcb%3A0xad9753dc82de20f0!2sInove+Uniformes!5e0!3m2!1spt-BR!2sbr!4v1480136801834', 'SIM', NULL, 'goiania', NULL, NULL, NULL),
(2, 'CUIABÁ', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15371.603799111688!2d-56.0996241!3d-15.5969401!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7609a600edca792a!2sInove+Uniformes!5e0!3m2!1spt-BR!2sbr!4v1480338374963', 'SIM', NULL, 'cuiaba', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_uniformes`
--

CREATE TABLE IF NOT EXISTS `tb_uniformes` (
  `iduniforme` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_uniformes`
--

INSERT INTO `tb_uniformes` (`iduniforme`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Uniformes de Saúdes', '2409201605381218447454.png', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'uniformes-de-saudes', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(8, 'Uniformes de Limpeza geral', '2409201605411332434728.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-de-limpeza-geral', NULL),
(9, 'Uniformes Cozinheiros', '2409201605401358107866.png', NULL, '', '', '', 'SIM', NULL, 'uniformes-cozinheiros', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  ADD PRIMARY KEY (`idatuacao`);

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  ADD PRIMARY KEY (`idequipe`);

--
-- Indexes for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Indexes for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  ADD PRIMARY KEY (`idface`);

--
-- Indexes for table `tb_galerias`
--
ALTER TABLE `tb_galerias`
  ADD PRIMARY KEY (`idgaleria`);

--
-- Indexes for table `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  ADD PRIMARY KEY (`id_galeriageral`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  ADD PRIMARY KEY (`id_galeriaservico`);

--
-- Indexes for table `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  ADD PRIMARY KEY (`id_galeriauniformes`);

--
-- Indexes for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  ADD PRIMARY KEY (`idgaleriaempresa`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Indexes for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Indexes for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  ADD PRIMARY KEY (`idparceiro`);

--
-- Indexes for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  ADD PRIMARY KEY (`idprodutoaplicacao`);

--
-- Indexes for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  ADD PRIMARY KEY (`idprodutosmodeloaceito`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Indexes for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  ADD PRIMARY KEY (`idsubcategoriaproduto`);

--
-- Indexes for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  ADD PRIMARY KEY (`idtipoveiculo`);

--
-- Indexes for table `tb_unidades`
--
ALTER TABLE `tb_unidades`
  ADD PRIMARY KEY (`idunidade`);

--
-- Indexes for table `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  ADD PRIMARY KEY (`iduniforme`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_atuacoes`
--
ALTER TABLE `tb_atuacoes`
  MODIFY `idatuacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `tb_clientes`
--
ALTER TABLE `tb_clientes`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_equipes`
--
ALTER TABLE `tb_equipes`
  MODIFY `idequipe` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_facebook`
--
ALTER TABLE `tb_facebook`
  MODIFY `idface` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tb_galerias`
--
ALTER TABLE `tb_galerias`
  MODIFY `idgaleria` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_galerias_geral`
--
ALTER TABLE `tb_galerias_geral`
  MODIFY `id_galeriageral` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=159;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=162;
--
-- AUTO_INCREMENT for table `tb_galerias_servicos`
--
ALTER TABLE `tb_galerias_servicos`
  MODIFY `id_galeriaservico` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_galerias_uniformes`
--
ALTER TABLE `tb_galerias_uniformes`
  MODIFY `id_galeriauniformes` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  MODIFY `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1292;
--
-- AUTO_INCREMENT for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tb_parceiros`
--
ALTER TABLE `tb_parceiros`
  MODIFY `idparceiro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_produtos_aplicacoes`
--
ALTER TABLE `tb_produtos_aplicacoes`
  MODIFY `idprodutoaplicacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_produtos_modelos_aceitos`
--
ALTER TABLE `tb_produtos_modelos_aceitos`
  MODIFY `idprodutosmodeloaceito` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  MODIFY `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_tipos_veiculos`
--
ALTER TABLE `tb_tipos_veiculos`
  MODIFY `idtipoveiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_unidades`
--
ALTER TABLE `tb_unidades`
  MODIFY `idunidade` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_uniformes`
--
ALTER TABLE `tb_uniformes`
  MODIFY `iduniforme` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
