
  <!--  ==============================================================  -->
  <!--  FLEXSLIDER PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container bg_branco_claro">
    <div class="row">
      <div class="col-12 carroucel_produtos padding0">
        <section class="slider">
          <div class="flexslider carousel">
            <ul class="slides">
              <?php
              $result = $obj_site->select("tb_produtos"," and exibir_home = 'SIM' order by rand() limit 8");
              if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){
                  $i=0;
                  ?>
                  <li class="col-3">
                    <div class="top45">
                      <div class="card">
                        <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                          <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 253, 202, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                        </a>
                        <div class="card-body bg_produtos_home">
                          <div class="desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[titulo]); ?></h1></div>
                          <div class=" top5 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($row[quantidade]); ?></h1></div>

                          <div class="col-8 padding0 ml-auto">
                            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>

                            <a href="javascript:void(0);" class="btn btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                              <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg" alt="">
                            </a>
                          </div>

                        </div>


                      </div>
                    </div>
                  </li>
                  <?php
                  if ($i==2) {
                    echo "<div class='clearfix'>  </div>";
                  }else {
                    $i++;
                  }
                }
              }
              ?>


            </ul>
          </div>
        </section>

      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--  FLEXSLIDER PRODUTOS -->
  <!--  ==============================================================  -->
