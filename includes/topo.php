
<div class="container-fluid ">
  <div class="row">

    <div class="bg_topo">  </div>

    <div class="container ">
      <div class="row">
        <?php
        if(empty($voltar_para)){
          $link_topo = Util::caminho_projeto()."/";
        }else{
          $link_topo = Util::caminho_projeto()."/".$voltar_para;
        }
        ?>

        <div class="col-4 top15 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img class="w-100" src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>

        <div class="col-8">


          <!-- ======================================================================= -->
          <!-- telefones e enderecos  -->
          <!-- ======================================================================= -->

          <div class="row contatos_topo">
            <?php if (!empty($config[endereco])): ?>
              <div class="col-7">
                <div class=" top20   pull-right media">
                  <img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_local.png" alt="">
                  <div class="media-body align-self-center">
                    <h2 class="mt-0"><?php Util::imprime($config[endereco]); ?></h2>
                  </div>
                </div>
              </div>
            <?php endif; ?>

            <div class="col-5">

              <div class="row">
                <?php if (!empty($config[telefone3])): ?>
                  <div class="col-7 top5">
                    <div class=" media pull-right ">
                      <img class="align-self-end d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
                      <div class="media-body ">
                        <p class="mt-0">FALE CONOSCO</p>
                        <h2 class="mb-0"><span><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone1]); ?>  </span></h2>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>

                <div class="col-5 top10">

                  <div class="dropdown">
                    <button class="btn btn_drop" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      LIGUE AGORA
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" disabled ><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></a>
                      <a class="dropdown-item" disabled><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></a>
                      <a class="dropdown-item" disabled><?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?></a>


                    </div>
                  </div>
                </div>
              </div>

            </div>


          </div>
          <!-- ======================================================================= -->
          <!-- telefones e enderecos  -->
          <!-- ======================================================================= -->


          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->
          <div class="col-12 top15 mont menu">
            <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
            <div id="navbar">
              <ul class="nav navbar-nav navbar-right align-self-center">
                <li >
                  <a class=" top10 <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/">HOME
                  </a>
                </li>

                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
                  </a>
                </li>



                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
                  </a>
                </li>

                <li>
                  <a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
                  </a>
                </li>


                <li >
                  <a class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>"
                    href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATO
                  </a>
                </li>

                <li>


                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->
                  <div class="">
                    <div class="dropdown show">
                      <a class=" btn_topo" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ORCAMENTO <img class="ml-1"src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_orcamento.png" alt="" />
                        <!-- <span class="badge badge-pill"><?php //echo count($_SESSION[solicitacoes_produtos])/*+count($_SESSION[solicitacoes_servicos])*/; ?></span> -->
                      </a>

                      <div class="dropdown-menu topo-meu-orcamento" aria-labelledby="dropdownMenuLink">

                        <table class="table  table-condensed">

                        <?php if(count($_SESSION[solicitacoes_servicos])+count($_SESSION[solicitacoes_produtos]) == 0): ?>

                          <div class="col-12">
                            <div class="text-center top10 bottom10">
                              <h5>NENHUMA ITEM SELECIONADO</h5>
                              <a href="<?php echo Util::caminho_projeto() ?>/produtos"
                                class="btn btn-primary  btn_azul col-8" >
                                ADICIONE PRODUTO
                              </a>
                            </div>
                          </div>



                        <?php else: ?>


                        <?php
                        if(count($_SESSION[solicitacoes_produtos]) > 0){
                          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                            ?>
                            <tr class="middle">
                              <div class="row lista-itens-carrinho">
                                <div class="col-2">
                                  <?php if(!empty($row[imagem])): ?>
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                  <?php endif; ?>
                                </div>
                                <div class="col-8 top5">
                                  <h1><?php Util::imprime($row[titulo]) ?></h1>
                                </div>
                                <div class="col-1">
                                  <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                                </div>
                                <div class="col-12"><div class="borda_carrinho top5">  </div> </div>
                              </div>
                            </tr>
                            <?php
                          }
                        }
                        ?>

                        <!--  ==============================================================  -->
                        <!--sessao adicionar servicos-->
                        <!--  ==============================================================  -->
                        <?php
                        if(count($_SESSION[solicitacoes_servicos]) > 0)
                        {
                          for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                          {
                            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                            ?>
                            <div class="row lista-itens-carrinho">
                              <div class="col-2">
                                <?php if(!empty($row[imagem])): ?>
                                  <!-- <img class="carrinho_servcos" src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" alt="" /> -->
                                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                                <?php endif; ?>
                              </div>
                              <div class="col-8 top5">
                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                              </div>
                              <div class="col-1">
                                <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="fa fa-trash"></i> </a>
                              </div>
                              <div class="col-12"><div class="borda_carrinho top5">  </div> </div>

                            </div>
                            <?php
                          }
                        }
                        ?>

                        <div class="col-12 text-right top10 bottom20">
                          <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn_orcamento" >
                            ENVIAR ORÇAMENTO
                          </a>
                        </div>

                      <?php   endif; ?>

                    </table>

                      </div>
                    </div>
                  </div>
                  <!--  ==============================================================  -->
                  <!--CARRINHO-->
                  <!--  ==============================================================  -->

                </li>

              </ul>
            </div><!--/.nav-collapse -->

          </div>
          <!--  ==============================================================  -->
          <!-- MENU-->
          <!--  ==============================================================  -->

        </div>



      </div>

    </div>
  </div>


</div>
