
<style media="screen">
.carroucel_produtos .carousel-indicators .active {
  background-color: #000;
}

.carroucel_produtos .carousel-indicators {
  position: absolute;
  right: 0;
  bottom: -20px;
}

.carroucel_produtos .controle .fa{
  color: #001d33;
}

.carroucel_produtos .carousel-control-next {
  right: -100px;
}


.carroucel_produtos .carousel-control-prev {
  left: -100px;
}

.carroucel_produtos .card-body{
  background: #d6d6d7;

}

.bg_produtos_home h1{
  line-height: 1.13rem /* 18/16 */;
  letter-spacing: -1px;
}

</style>
<?php
$qtd_itens_linha = 4; // quantidade de itens por linha
$id_slider = "carouselExampleIndicators1";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="mont carroucel_produtos padding0 top20 bottom50 carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_produtos","and exibir_home = 'SIM' limit 8");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_produtos", "limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>
        <div class="carousel-item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>



              <div class="col-3 pull-left top45">
                <div class="card">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($linha[$c_temp][url_amigavel]); ?>" title="<?php Util::imprime($linha[$c_temp][titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 253, 202, array("class"=>"w-100", "alt"=>"$linha[$c_temp][titulo]")) ?>
                  </a>
                  <div class="card-body bg_produtos_home">
                    <div class="desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($linha[$c_temp][titulo]); ?></h1></div>
                    <div class=" top5 desc_titulo_home text-uppercase"><h1 ><?php Util::imprime($linha[$c_temp][quantidade]); ?></h1></div>

                    <div class="col-10 padding0 ml-auto">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($linha[$c_temp][url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">MAIS DETALHES</a>

                      <a href="javascript:void(0);" class="btn btn_produtos_home" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($linha[$c_temp][0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg" alt="">
                      </a>
                    </div>

                  </div>


                </div>
              </div>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>



  <a class="carousel-control-prev controle" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <img src="<?php echo Util::caminho_projeto() ?>/imgs/seta_left.png" alt="">
    <span class="sr-only">prev</span>
  </a>
  <a class="carousel-control-next controle" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <img src="<?php echo Util::caminho_projeto() ?>/imgs/seta_right.png" alt="">
    <span class="sr-only">Next</span>
  </a>
</div>
