
<div class="container-fluid rodape">
	<div class="row pb20">

		<!-- <a href="#" class="scrollup">scrollup</a> -->


		<div class="container">
			<div class="row top55">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img class=" w-100" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->



				<div class="col-2 padding0 top10 menu_rodape ">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav">
						<li class="nav-item col-6 padding0">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item col-6 padding0">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
							</a>
						</li>

						<li class="nav-item col-6 padding0">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>


						<li class="nav-item col-6 padding0">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS
							</a>
						</li>



						<li class="nav-item col-6 padding0">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>



						<li class="nav-item col-6 padding0">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
							</a>
						</li>

					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>







				<div class="col-5 endereco_rodape">
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="row">
						<div class="col-12 bottom20 top10">	<h1>FALE CONOSCO</h1>	<b>LIGUE AGORA</b></div>


						<div class="col-5 media">
							<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
							<div class="media-body align-self-center">
								<h3 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h3>

							</div>
						</div>



						<?php if (!empty($config[telefone2])): ?>
							<div class="col media">
								<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
								<div class="media-body align-self-center">
									<h3 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h3>
								</div>
							</div>
						<?php endif; ?>

						<?php if (!empty($config[telefone3])): ?>
							<div class="col-5 media">
								<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
								<div class="media-body align-self-center">
									<h3 class="mt-0 mr-3"><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></h3>
								</div>
							</div>
						<?php endif; ?>

						<?php if (!empty($config[telefone4])): ?>
							<div class="col media">
								<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/telefone_rodape.png" alt="">
								<div class="media-body align-self-center">
									<h3 class="mt-0 mr-3"><?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?></h3>
								</div>
							</div>
						<?php endif; ?>



						<!-- ======================================================================= -->
						<!-- ONDE ESTAMOS   -->
						<!-- ======================================================================= -->
						<div class="col-12 top5">
							<div class="media top15">
								<img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/endereco_rodape.png" alt="">
								<div class="media-body">
									<p class="mt-0"><?php Util::imprime($config[endereco]); ?></p>
								</div>
							</div>

						</div>
						<!-- ======================================================================= -->
						<!-- ONDE ESTAMOS   -->
						<!-- ======================================================================= -->


					</div>

					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
				</div>










				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->

				<div class="col-2 redes top40">
					<div class="row">

						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->
						<div class="col-7 padding0 top35 text-right redes">
								<?php if ($config[facebook] != "") { ?>
									<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
										<i class="fa fa-facebook-official fa-2x"></i>
									</a>
								<?php } ?>

								<?php if ($config[instagram] != "") { ?>
									<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
										<i class="fa fa-instagram fa-2x"></i>
									</a>
								<?php } ?>

								<?php if ($config[google_plus] != "") { ?>
									<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
										<i class="fa fa-google-plus-official  fa-2x "></i>
									</a>
								<?php } ?>
						</div>
						<!-- ======================================================================= -->
						<!-- redes sociais    -->
						<!-- ======================================================================= -->


						<div class="col-5">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>



					</div>
				</div>
				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright  SM PLASTIC</h5>
		</div>
	</div>
</div>
